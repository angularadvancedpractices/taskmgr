# Angular Task Manager
Task Manager is a web application developed in Angular 6 and Material style, NgRX(Redux) and RxJS, with Express & MongoDB as back. The authentication is using tokens based on JWT.
The project is continuously developed and added new front-end techniques.
# Getting started
## what you need to start
- You basically need to know typescript and ES6
- You already have some experience developing Angular before and would like to get some inspiration on how to build a real world Angular Authentication staff or
- would like to know how Angular can be combined with Redux (NgRX) or 
- would like to know something regarding Angular Router, RXJS, Angular Material, etc.
## prework
- install git on your linux, mac or Windows PC
- install node on your linux, mac or Windows PC
- install angular cli globally using npm or yarn
- install pm2 or forever globally (optional)
## installing
- clone the repo or download git clone https://yiyuxu@bitbucket.org/angularadvancedpractices/taskmgr.git
- get into the taskmgr directory, and npm i
- ng serve
- get into the server directory, and run npm i, then run node index.js if you installed forever, then run forever start index.js
# Live Demo
http://178.157.90.179:8888
# tutorial
will publish soon...
Please refer to wiki https://bitbucket.org/angularadvancedpractices/taskmgr/wiki/Home