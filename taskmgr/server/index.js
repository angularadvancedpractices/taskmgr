// const db = require('./data/db');
// const mongo = require('mongodb');
// const MongoStore = require('connect-mongo')(session);
// const userRoutes = require('./routes(old)/userRoutes');
// const projectRoutes = require('./routes(old)/projectRoutes');
// const authRoutes = require('./routes(old)/authRoutes');
const express = require('express');
const app = express();
const session = require('express-session');
const bodyParser = require('body-parser');
const cors = require('cors');
const urlencoded = bodyParser.urlencoded({extended: false});
const jwtAuth = require('./jwt');

const userRouter = require('./routes/user');
const projectRouter = require('./routes/project');
const authRouter = require('./routes/auth');
const taskListRouter = require('./routes/task-list');
const taskRouter = require('./routes/task');
// mongoose configuration
const mongoose = require('mongoose');
//Set up default mongoose connection
const mongoDB = 'mongodb://127.0.0.1/angular_taskmgr';
mongoose.connect(mongoDB, {useNewUrlParser: true});
// Get Mongoose to use the global promise library
mongoose.Promise = global.Promise;
//Get the default connection
const db = mongoose.connection;
//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
// save the session in MongoDB database
// const mongoStoreInstance = new MongoStore({
//   url: 'mongodb://localhost/angular_taskmgr'
// });
// app.use(session({
//     // save the session in MongoDB database
//     store: mongoStoreInstance,
//     secret: '~~~',
//     resave: false,
//     saveUninitialized: true,
//     cookie: {
//       maxAge: 30 * 60 * 1000 * 24,
//       secure: false,
//       httpOnly: false,
//     }
//   })
// );
app.disable('etag');
app.use(cors());
app.use(urlencoded);
app.use(bodyParser.json());
app.use(jwtAuth);

app.use('/auth', authRouter);
app.use('/users', userRouter);
app.use('/projects', projectRouter);
app.use('/tasks', taskRouter);
app.use('/task-lists', taskListRouter);

// app.post('/register', (req, res) => {userRoutes.register(req, res, db, md5)});
//
// app.post('/login', (req, res) => authRoutes.login(req, res, db, jwt));
//
// app.post('/logout', (req, res) => userRoutes.logout(req, res, mongoStoreInstance));
//
// app.get('/projects', (req, res) => projectRoutes.getProjectsOfUser(req, res, db, mongo));
//
// app.post('/projects/invite', (req, res) => projectRoutes.inviteUsers(req, res, db, mongo));
//
// app.get('/users/search', (req, res) => userRoutes.searchUsersByKeyword(req, res, db));
//
// app.get('/users', (req, res) => userRoutes.getUsersByProjectId(req, res, db, mongo));

// app.listen('3000', function () {
//   db.settings('127.0.0.1', '27017', 'angular_taskmgr')
// });

app.listen('3000');
