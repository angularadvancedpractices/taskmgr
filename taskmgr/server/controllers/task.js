const Task = require('../models/task').Task;
const TaskList = require('../models/task-list').TaskList;

exports.loadTasksByTaskListId = function (req, res) {
  const taskListId = req.params.taskListId;
  TaskList
    .findOne({_id: taskListId})
    .populate('tasks')
    .exec()
    .then(taskList => res.status(200).json(taskList.tasks))
    .catch(err => res.status(500).json(err));
};

exports.newTask = function (req, res) {
  const reqParams = req.body;
  const newTask = new Task({
    taskListId: reqParams.taskListId,
    name: reqParams.task.name,
    description: reqParams.task.desc,
    priority: reqParams.task.priority,
    owner: reqParams.task.ownerId,
    participants: reqParams.task.participantIds,
    startDate: reqParams.task.startDate,
    dueDate: reqParams.task.dueDate,
    createDate: reqParams.task.createDate,
    completed: reqParams.task.completed,
    remark: reqParams.task.remark
  });
  TaskList
    .findOne({_id: reqParams.taskListId}, (err, taskList) => {
      if (taskList) {
        newTask.save((err, task) => {
          if (err) {
            return res.status(500).json(err)
          } else {
            taskList.tasks.push(task._id);
            taskList.save((err, _) => {
              if (err) {
                return res.status(500).json(err)
              } else {
                return res.status(200).json(task)
              }
            });
          }
        });
      } else {
        return res.status(404).json(`task list cannot be found with projectId: ${req.body.taskListId} `)
      }
    });
};

exports.updateTask = (req, res) => {
  const taskId = req.params.taskId;
  const updateTask = req.body.task;
  console.log(updateTask);
  Task.findOneAndUpdate({_id: taskId}, {
    name: updateTask.name,
    description: updateTask.description,
    priority: updateTask.priority,
    startDate: updateTask.startDate,
    dueDate: updateTask.dueDate,
    owner: updateTask.owner,
    participants: updateTask.participants,
    remark: updateTask.remark,
  }, {new: true})
    .exec()
    .then(task => {
      console.log(task);
      return res.status(200).json(task)
    });
};

exports.completeTask = (req, res) => {
  const taskId = req.params.taskId;
  const completed = req.body.completed;
  Task.findOneAndUpdate({_id: taskId}, {
    completed: Boolean(completed)
  }, {new: true})
    .exec()
    .then(task => {
      // console.log(task.completed);
      return res.status(200).json(task)
    })
};

exports.moveTask = (req, res) => {
  const task = req.body.task;
  const targetListId = req.body.targetListId;
  Task
  // 找到需要替换的task然后替换task的list id为target list id
    .findOneAndUpdate({_id: task._id}, {taskListId: targetListId}, {new: true})
    .exec()
    .then(task => {
      // 成功
      TaskList
      // 找到原task所在的taskList
        .find({tasks: task._id}, (err, taskList) => {
          // 从原tasklist中删除此task
          // console.log(taskList);
          taskList[0].tasks.pull(task._id);
          taskList[0].save((err, updatedTaskList) => {
            if (err) {
              return res.status(500).json(err);
            } else {
              // 找到目标tasklist
              TaskList
                .find({_id: targetListId}, (err, targetList) => {
                // console.log(targetList);
                  // 将此task保存到新的taskList中
                  targetList[0].tasks.push(task._id);
                  targetList[0].save((err, t) => {
                    if (err) {
                      return res.json(500).json(err);
                    }
                    return res.status(200).json({
                      updatedTask: task,
                      taskListRemovedTaskId: updatedTaskList,
                      taskListAddedTaskId: targetList
                    })
                  })
                })
            }
          })
        })
    })
    .catch(err => res.status(500).json(err));
};

exports.moveAllTask = (req, res) => {
  const srcListId = req.body.srcListId;
  const targetListId = req.body.targetListId;
  TaskList
    .findOne({_id: srcListId})
    .populate('tasks')
    .exec()
    .then(taskList => {
      const tasks = taskList.tasks;
      TaskList.updateOne({_id: srcListId}, {$set: {tasks: []}}, function (err, result) {
        if (err) {
          return res.status(500).json(err);
        } else {
          TaskList.updateOne({_id: targetListId}, {$set: {tasks: tasks}}, function (err, result) {
            if (err) {
              return res.status(500).json(err)
            } else {
              Task.updateMany({}, {$set: {taskListId: targetListId}}, function (err, result) {
                if (err) {
                  return res.status(500).json(err);
                }
                return res.status(200).json({
                  tasks,
                  srcListId,
                  targetListId
                });
              })
            }
          })
        }
      })
    })
};

exports.loadAllTasks = (req, res) => {
  Task.find({}, (err, result) => {
    if (err) {
      return res.status(500).json(err)
    } else {
      return res.status(200).json(result);
    }
  })
};
