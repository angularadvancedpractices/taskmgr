const Project = require('../models/project').Project;
const TaskList = require('../models/task-list').TaskList;
const Task = require('../models/task').Task;

exports.getProjectById = (req, res) => {
  const projectId = req.query.id;
  Project.findOne({_id: projectId})
    .exec()
    .then(projects => {
      res.status(200).json(projects);
    })
    .catch(err => console.log(err));
};

exports.getProjectsByUserId = function (req, res) {
  const userId = req.query.userId;
  if (userId) {
    Project
      .find({members: userId})
      .exec()
      .then(projects => {
        console.log(projects);
        return res.status(200).json(projects);
      })
      .catch(err => console.log(err))
  }
};

exports.updateProjectMembers = async function (req, res) {
  const projectId = req.body.projectId;
  const projectMembers = req.body.inviteUsers.map(u => u._id);
  Project
  // return the updated document
    .findOneAndUpdate({_id: projectId}, {members: projectMembers}, {new: true})
    .exec()
    .then(updatedProject => {
      res.status(200).json(updatedProject);
    })
    .catch(err => console.log(err));
};

exports.newProject = function (req, res) {
  const reqParams = req.body;
  // console.log(reqParams);
  const newProject = new Project({
    name: reqParams.name,
    desc: reqParams.desc,
    coverImg: reqParams.coverImg,
    members: reqParams.members
  });
  newProject.save().then(project => res.status(200).json(project)).catch(err => res.status(500).json(err));
};

exports.updateProject = function (req, res) {
  const projectId = req.params.id;
  const reqParams = req.body;
  console.log(projectId);
  Project
  // return the updated document
    .findOneAndUpdate({_id: projectId}, {name: reqParams.name, desc: reqParams.desc}, {new: true})
    .exec()
    .then(updatedProject => {
      res.status(200).json(updatedProject);
    })
    .catch(err => console.log(err));
};

exports.deleteProject = function (req, res) {
  const projectId = req.params.id;
  Project
    .findOne({_id: projectId})
    .populate('taskLists')
    .exec()
    .then(project => {
      const taskLists = project.taskLists;
      taskLists.forEach(taskList => {
        TaskList
          .findOne({_id: taskList._id})
          .populate('tasks')
          .exec()
          .then(_ => {
            const tasks = taskList.tasks;
            tasks.forEach(task => {
              Task.findOneAndDelete({_id: task._id}).exec();
            });
            TaskList.findOneAndDelete({_id: taskList._id}, function (err, result) {
              if (err) {
                res.status(500).json({
                  err,
                  'error tasklist': taskList
                });
              }
            });
          });
      });
      Project.findOneAndDelete({_id: projectId}, function (err, result) {
        if (err) {
          res.status(500).json({
            err,
            'error project': project
          });
        } else {
          return res.json(project);
        }
      })
    })

};
