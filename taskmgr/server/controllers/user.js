const User = require('../models/user').User;
const Project = require('../models/project').Project;

exports.getUsersByProjectId = async (req, res) => {
  const projectId = req.query.projectId;
  // console.log(projectId);
  Project.findOne({_id: projectId})
    .populate('members')
    .exec()
    // 这里还是project只是带上了users这个数组给前端
    .then(project => {
      res.status(200).json(project.members);
    })
    .catch(err => console.log(err))
};

exports.searchUsersByKeyword = (req, res) => {
  const keyword = req.query.keyword;
  if (keyword) {
    User.find({email: new RegExp(keyword)}, function (err, users) {
      if (err) {
        console.log(err);
      } else {
        res.status(200).json(users);
      }
    })
  }
};


