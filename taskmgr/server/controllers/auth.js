const nodeMailer = require('nodemailer');
const User = require('../models/user').User;
const jwt = require('jsonwebtoken');
const {secretKey} = require('../constant');

exports.login = (req, res) => {
  const email = req.body.params.email;
  const password = req.body.params.password;
  User.findOne({email, password})
    .exec()
    .then(user => {
      if (user) {
        const token = jwt.sign({username: user.name}, secretKey, {
          expiresIn: 60 * 60 * 72
        });
        return res.status(200).json({
          token: token,
          user: user
        });
      } else {
        return res.status(401).json({
          msg: 'username or password not correct'
        })
      }
    })
    .catch(err => console.log(err));
};


exports.register = (req, res) => {
  const userInfo = req.body.user;
  // console.log(userInfo);
  const newUser = new User({
    email: userInfo.email,
    name: userInfo.name,
    password: userInfo.password,
    dateOfBirth: userInfo.dateOfBirth,
    avatar: userInfo.avatar
  });
  newUser.save((err, user) => {
    if (err) {
      return res.status(500).json(err);
    } else {
      return res.status(200).json(user);
    }
  })
};

exports.reset = (req, res) => {
  const userId = req.body.userModel.userId;
  const password = req.body.userModel.password;
  User
    .findOneAndUpdate({_id: userId}, {password: password}, (err, user) => {
      if (err) {
        return res.status(500).json(err);
      } else {
        return res.status(200).json(user);
      }
    })
};

exports.forget = (req, res) => {
  const email = req.body.email;
  User
    .findOne({email: email})
    .exec()
    .then(user => {
      if (user === null) {
        return res.status(404).json('no such user found');
      } else {
        const transporter = nodeMailer.createTransport({
          service: 'gmail',
          auth: {
            user: 'ataskmanager@gmail.com',
            pass: 'Xyyzzj23*'
          }
        });

        const mailOptions = {
          from: 'xuyiyu1992@gmail.com',
          to: email,
          subject: 'Reset your password for Angular Task manager',
          html:
          '<h2>Reset your password</h2>' +
          '<p>Please click the link below to reset your password</p>' +
          `<a href="http://localhost:4200/reset/${user._id}" target="_blank">reset</a>` +
          '<br>' +
          '<p>Angular Task manager team</p>'
        };

        transporter.sendMail(mailOptions, (err, info) => {
          if (err) {
            console.log(err);
            transporter.close();
          } else {
            transporter.close();
            return res.status(200).json('message sent')
          }
        })
      }
    })
    .catch(err => console.log(err));
};
