const TaskList = require('../models/task-list').TaskList;
const Project = require('../models/project').Project;

exports.getTaskListsByProjectId = function (req, res) {
  const projectId = req.params.projectId;
  Project
    .findOne({_id: projectId})
    .populate('taskLists')
    .exec()
    .then(project => res.status(200).json(project.taskLists))
    .catch(err => res.status(500).json(err))
};

exports.newTaskList = function (req, res) {
  const reqParams = req.body;
  // console.log(reqParams);
  const newTaskList = new TaskList({
    name: reqParams.name,
    order: reqParams.order,
    projectId: reqParams.projectId
  });
  Project
    .findOne({_id: req.body.projectId}, (err, project) => {
      if (project) {
        newTaskList.save((err, taskList) => {
          if (err) {
            return res.status(500).json(err)
          } else {
            project.taskLists.push(taskList._id);
            project.save((err, project) => {
              if (err) {
                return res.status(500).json(err)
              } else {
                console.log(taskList);
                return res.status(200).json(taskList)
              }
            });
          }
        });
      } else {
        return res.status(404).json(`project cannot be found with projectId: ${req.body.projectId} `)
      }
    });
};

exports.updateTaskList = (req, res) => {
  const taskListId = req.params.taskListId;
  // console.log(req.params.taskListId);
  // console.log(req.body);
  const newName = req.body.name;
  TaskList
    .findOneAndUpdate({_id: taskListId}, {name: newName}, {new: true}, (err, taskList) => {
      if (err) {
        return res.status(500).json(err);
      } else {
        console.log(taskList);
        return res.status(200).json(taskList);
      }
    })
};

exports.swapTaskListOrder = (req, res) => {
  const srcId = req.body.src._id;
  const srcOrder = req.body.src.order;
  const targetId = req.body.target._id;
  const targetOrder = req.body.target.order;
  TaskList.findOneAndUpdate({_id: srcId}, {order: targetOrder}, {new: true}, (err, srcList) => {
    if (err) {
      return res.status(500).json(err);
    } else {
      TaskList.findOneAndUpdate({_id: targetId}, {order: srcOrder}, {new: true}, (err, targetList) => {
        if (err) {
          return res.status(500).json(err);
        } else {
          return res.status(200).json([srcList, targetList]);
        }
      })
    }
  })
};

exports.deleteTaskList = (req, res) => {
  const taskListId = req.params.taskListId;
  const projectId = req.params.projectId;
  // console.log(taskListId);
  TaskList.findOneAndDelete({_id: taskListId}, (err, taskList) => {
    if (err) {
      return res.status(500).json(err);
    } else {
      Project.findOne({_id: projectId}, (err, project) => {
        if (err) {
          return res.status(500).json(err)
        } else {
          // console.log(project);
          project.taskLists.pull(taskListId);
          project.save((err, project) => {
            if (err) {
              return res.status(500).json(err);
            } else {
              return res.status(200).json(taskList)
            }
          })
        }
      })
    }
  })
};

exports.loadAllTaskLists = (req, res) => {
  TaskList.find({}, (err, result) => {
    if (err) {
      return res.status(500).json(err)
    } else {
      return res.status(200).json(result);
    }
  })
};
