// module.exports = {
//   getProjectsOfUser: (req, res, db, mongo) => {
//     const userId = req.query.userId;
//     db.findResult('projects', {query: {members: mongo.ObjectId(userId)}}, (err, result) => {
//       return res.status(200).send({
//         projects: result
//       });
//     })
//   },
//   inviteUsers: (req, res, db, mongo) => {
//     const projectId = req.body.projectId;
//     const users = req.body.users;
//     const userIds = users.map(u => mongo.ObjectId(u._id));
//     const params = {
//       recordId: mongo.ObjectId(projectId),
//       updateField: 'members',
//       pushData: userIds
//     };
//     db.pushArrayRecord('projects', params, (updatedProjectErr, updatedProject) => {
//       console.log(updatedProject);
//       if (updatedProject.ok) {
//         let result = {
//           project: {},
//           users: []
//         };
//         result.project = updatedProject.value;
//         users.forEach((user, index) => {
//           const userParams = {
//             recordId: mongo.ObjectId(user._id),
//             updateField: 'projectIds',
//             pushData: mongo.ObjectId(projectId)
//           };
//           db.pushArrayRecord('users', userParams, (updatedUserErr, updatedUser) => {
//             if (updatedUser.ok) {
//               const r = updatedUser.value;
//               result.users.push({user: r});
//             }
//             if (index + 1 === users.length) {
//               return res.status(200).json(result);
//             }
//           });
//         });
//       }
//
//     })
//   }
// };
