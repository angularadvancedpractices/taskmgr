const mongoose = require('mongoose'),
  ObjectId = mongoose.Types.ObjectId,
  Schema = mongoose.Schema,
  Project = require('./project').Project;

const UserSchema = new Schema({
  name: String,
  avatar: String,
  email: String,
  dateOfBirth: Date,
  password: {type: String, select: false}
});
const User = mongoose.model('User', UserSchema);
module.exports = {
  User: User
};
