const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const TaskSchema = new Schema({
  taskListId: {type: String, required: true},
  name: {type: String, required: true},
  description: {type: String, required: true},
  priority: {type: Number, required: true},
  owner: {type: Schema.Types.ObjectId, ref: 'User', required: true},
  participants: [{type: Schema.Types.ObjectId, ref: 'User', default: []}],
  startDate: {type: Date, required: true},
  dueDate: {type: Date, required: true},
  createDate: {type: Date, required: true},
  completed: {type: Boolean, required: true},
  remark: {type: String}
});
const Task = mongoose.model('Task', TaskSchema);
module.exports = {
  Task
};
