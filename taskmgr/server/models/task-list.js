const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const TaskListSchema = new Schema({
  name: {type: String, required: true},
  order: {type: Number, required: true},
  tasks: [{type: Schema.Types.ObjectId, ref: 'Task', default: []}],
  projectId: {type: String, required: true}
});
const TaskList = mongoose.model('TaskList', TaskListSchema);
module.exports = {
  TaskList
};
