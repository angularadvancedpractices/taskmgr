const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const ProjectSchema = new Schema({
  name: {type: String, required: true},
  desc: {type: String, required: true},
  coverImg: {type: String, required: true},
  members: [{type: Schema.Types.ObjectId, ref: 'User', default: []}],
  taskLists: [{type: Schema.Types.ObjectId, ref: 'TaskList', default: []}]
});
const Project = mongoose.model('Project', ProjectSchema);
module.exports = {
  Project: Project
};
