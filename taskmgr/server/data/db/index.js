const MongoClient = require('mongodb').MongoClient;

let dbUrlFromSetting = '';
let dbPortNumberFromSetting = '';
let dbNameFromSetting = '';

const dbName = 'angular_taskmgr';

function _connectDB(callback) {
  // default url
  let url = '';
  if (dbUrlFromSetting === '' || dbPortNumberFromSetting === '' || dbNameFromSetting === '') {
    url = 'mongodb://localhost:27017/angular_taskmgr'
  } else {
    url = 'mongodb://' + dbUrlFromSetting + ':' + dbPortNumberFromSetting + '/' + dbNameFromSetting;
  }
  MongoClient.connect(url, {useNewUrlParser: true}, (err, client) => {
    if (err) {
      throw err;
    }
    callback(err, client)
  });
}

exports.settings = function (dbUrl, dbPortNumber, dbName) {
  if (arguments.length !== 3) {
    throw 'Arguments number must be 3'
  }
  dbUrlFromSetting = dbUrl;
  dbPortNumberFromSetting = parseInt(dbPortNumber);
  dbNameFromSetting = dbName;
};

exports.findOneResult = function (collectionName, params, callback) {
  if (params.fields) {
    _connectDB((err, client) => {
      client.db(dbName).collection(collectionName).findOne(params.query, params.fields, function (err, result) {
        return callback(err, result)
      });
    })
  } else {
    _connectDB((err, client) => {
      client.db(dbName).collection(collectionName).find(params).toArray(function (err, result) {
        return callback(err, result)
      });
    })
  }
};

exports.findResult = function (collectionName, params, callback) {
  _connectDB((err, client) => {
    client.db(dbName).collection(collectionName).find(params.query).toArray(function (err, result) {
      return callback(err, result)
    });
  })
};

exports.getAllRecordCount = function (collectionName, callback) {
  _connectDB(function (err, client) {
    client.db(dbName).collection(collectionName).find({}).count().then(function (count) {
      callback(err, count)
    })
  })
};

exports.pushArrayRecord = function (collectionName, params, callback) {
  _connectDB(function (err, client) {
    const recordId = params.recordId;
    const updateField = params.updateField;
    const pushData = params.pushData;
    // client.db(dbName).collection(collectionName).findOneAndUpdate({'_id': recordId}, {$addToSet: {[updateField]: {$each: pushData}}}, {returnOriginal: false}, function (err, result) {
    //   callback(err, result)
    // })
    client.db(dbName).collection(collectionName).findOneAndUpdate({'_id': recordId}, {[updateField]: pushData}, {returnOriginal: false}, function (err, result) {
      callback(err, result)
    })
  })
};

exports.pushRecord = function (collectionName, params, callback) {
  _connectDB(function (err, client) {
    const recordId = params.recordId;
    const updateField = params.updateField;
    const pushData = params.pushData;
    client.db(dbName).collection(collectionName).findOneAndUpdate({'_id': recordId}, {$addToSet: {[updateField]: pushData}}, {returnOriginal: false}, function (err, result) {
      callback(err, result)
    })
  })
};
