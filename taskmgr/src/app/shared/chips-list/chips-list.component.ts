import {Component, forwardRef, Input, OnInit} from '@angular/core';
import {
  ControlValueAccessor, FormBuilder, FormControl, FormGroup, NG_VALIDATORS,
  NG_VALUE_ACCESSOR
} from '@angular/forms';
import {User} from '../../domain/user.model';
import {Observable} from 'rxjs/index';
import {debounceTime, distinctUntilChanged, filter, map, switchMap} from 'rxjs/internal/operators';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-chips-list',
  templateUrl: './chips-list.component.html',
  styleUrls: ['./chips-list.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ChipsListComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => ChipsListComponent),
      multi: true
    }
  ]
})
export class ChipsListComponent implements OnInit, ControlValueAccessor {

  @Input() multiple = true;
  @Input() removable = true;
  @Input() placeholderText = 'Please type member email';
  @Input() label = 'Add/Edit Member';
  @Input() projectUsers: User[] = [];

  form: FormGroup;
  items: User[] = [];
  memberResults$: Observable<User[]>;
  projectUserIds: string[] = [];

  private propagateChange = (_: any) => {};

  registerOnTouched(fn: any): void {
    return null;
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  writeValue(obj: User[]): void {
    if (obj && this.multiple && obj.indexOf(undefined) === -1) {
      console.log(obj);
      const userEntities = obj.reduce((e, c) => ({...e, c}), {});
      if (this.items) {
        const remaining = this.items.filter(item => !userEntities[item._id]);
        // take the common part out
        this.items = [...remaining, ...obj];
      }
    } else if (obj && !this.multiple) {
      this.items = [...obj];
    }
  }

  constructor(private fb: FormBuilder, private userService: UserService) {
  }

  ngOnInit() {
    this.projectUserIds = this.projectUsers.map(u => u._id);
    this.form = this.fb.group({
      memberSearch: [''],
    });
    if (this.projectUserIds.length === 0) {
      this.memberResults$ = this.form.get('memberSearch').valueChanges
        .pipe(
          debounceTime(300),
          distinctUntilChanged(),
          filter(s => s && s.length > 1),
          switchMap((str: string) => this.userService.searchUsers(str))
        );
    } else {
      this.memberResults$ = this.form.get('memberSearch').valueChanges
        .pipe(
          debounceTime(300),
          distinctUntilChanged(),
          filter(s => s && s.length > 1),
          switchMap((str: string) => this.userService.searchUsers(str)),
          map(users => users.filter(user => this.projectUserIds.indexOf(user._id) > -1))
        );
    }
  }

  validate(c: FormControl): { [key: string]: any } {
    return this.items ? null : {
      chipListInvalid: true
    };
  }

  removeMember(member: User) {
    const ids = this.items.map(item => item._id);
    const i = ids.indexOf(member._id);
    if (this.multiple) {
      this.items = [...this.items.slice(0, i), ...this.items.slice(i + 1)];
    } else {
      this.items = [];
    }
    this.form.patchValue({memberSearch: ''});
    this.propagateChange(this.items);
  }

  handleMemberSelection(member: User) {
    if (this.items.map(item => item._id).indexOf(member._id) !== -1) {
      return;
    }
    this.items = this.multiple ? [...this.items, member] : [member];
    this.form.patchValue({memberSearch: member.name});
    this.propagateChange(this.items);
  }

  displayUser(user: User): string {
    return user ? user.name : '';
  }

  get displayInput() {
    return this.multiple || this.items.length === 0;
  }
}
