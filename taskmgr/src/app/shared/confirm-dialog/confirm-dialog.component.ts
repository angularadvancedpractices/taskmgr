import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-confirm-dialog',
  template: `
    <form>
      <h2 mat-dialog-title>{{dialogTitle}}</h2>
      <div mat-dialog-content>{{dialogContent}}</div>
      <mat-dialog-actions>
        <button mat-raised-button type="button" (click)="onDialogClose(true)">OK</button>
        <span></span>
        <button mat-button type="button" mat-dialog-close (click)="onDialogClose(false)">Close</button>
      </mat-dialog-actions>
    </form>
  `,
  styles: [`
    :host {
      min-width: 15em;
    }
    mat-dialog-actions {
      display: flex;
      flex-direction: row;
      justify-content: space-between;
    }
  `]
})
export class ConfirmDialogComponent implements OnInit {

  dialogTitle;
  dialogContent;

  constructor(private matRef: MatDialogRef<ConfirmDialogComponent>,
              @Inject(MAT_DIALOG_DATA) private dialogData: any) {
  }

  ngOnInit() {
    this.dialogTitle = this.dialogData.title;
    this.dialogContent = this.dialogData.content;
  }

  onDialogClose(result) {
    this.matRef.close(result);
  }

}
