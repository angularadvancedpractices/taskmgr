import {Component, forwardRef, Input, OnDestroy, OnInit} from '@angular/core';
import {
  ControlValueAccessor, FormBuilder, FormControl, FormGroup, NG_VALIDATORS,
  NG_VALUE_ACCESSOR
} from '@angular/forms';
import {debounceTime, distinctUntilChanged, filter, map, startWith} from 'rxjs/internal/operators';
import {combineLatest, merge, Subscription} from 'rxjs/index';
import * as sub_days from 'date-fns/sub_days';
import * as sub_months from 'date-fns/sub_months';
import * as sub_years from 'date-fns/sub_years';
import * as difference_in_days from 'date-fns/difference_in_days';
import * as difference_in_months from 'date-fns/difference_in_months';
import * as difference_in_years from 'date-fns/difference_in_years';
import * as is_before from 'date-fns/is_before';
import * as parse from 'date-fns/parse';
import * as format from 'date-fns/format';
import {isValidDate} from '../../utils/date.util';

export enum AgeUnit {
  Year = 0,
  Month,
  Day
}

export interface Age {
  age: number;
  unit: AgeUnit;
}

@Component({
  selector: 'app-age-input',
  templateUrl: './age-input.component.html',
  styleUrls: ['./age-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AgeInputComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => AgeInputComponent),
      multi: true
    }
  ]
})
export class AgeInputComponent implements ControlValueAccessor, OnInit, OnDestroy {

  @Input() daysTop = 90;
  @Input() daysBot = 0;
  @Input() monthsTop = 24;
  @Input() monthsBot = 1;
  @Input() yearsTop = 150;
  @Input() yearsBot = 1;
  @Input() format = 'YYYY-MM-DD';
  @Input() debounceTime = 300;
  selectedUnit = AgeUnit.Year;
  sub: Subscription;
  ageUnits = [
    {value: AgeUnit.Year, label: 'Years old'},
    {value: AgeUnit.Month, label: 'Months old'},
    {value: AgeUnit.Day, label: 'Days old'}
  ];
  form: FormGroup;

  private propagateChange = (_: any) => {
  }

  registerOnTouched(fn: any): void {
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  writeValue(obj: any): void {
    if (obj) {
      const date = format(obj, this.format);
      this.form.get('birthday').patchValue(date);
      const age = this.toAge(date);
      this.form.get('age').get('ageNum').patchValue(age.age);
      this.form.get('age').get('ageUnit').patchValue(age.unit);
    }
  }

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      birthday: ['', this.validateDate],
      age: this.fb.group({
        ageNum: [],
        ageUnit: [AgeUnit.Year],
      }, {validator: this.validateAge('ageNum', 'ageUnit')})
    });
    const birthday = this.form.get('birthday');
    const ageNum = this.form.get('age').get('ageNum');
    const ageUnit = this.form.get('age').get('ageUnit');

    const birthday$ = birthday.valueChanges.pipe(
      map(d => {
        return {date: d, from: 'birthday'};
      }),
      filter(_ => birthday.valid),
      debounceTime(this.debounceTime),
      distinctUntilChanged());

    const ageNum$ = ageNum.valueChanges.pipe(
      startWith(ageNum.value),
      debounceTime(this.debounceTime),
      distinctUntilChanged());

    const ageUnit$ = ageUnit.valueChanges.pipe(
      startWith(ageUnit.value),
      debounceTime(this.debounceTime),
      distinctUntilChanged());

    const age$ = combineLatest(ageNum$, ageUnit$).pipe(
      map(results => {
        return this.toDate({age: results[0], unit: results[1]});
      }),
      map(d => {
        return {date: d, from: 'age'};
      }),
      filter(_ => this.form.get('age').valid));

    const merged$ = merge(birthday$, age$).pipe(filter(_ => this.form.valid));

    this.sub = merged$.subscribe(d => {
      // console.log(d);
      const age = this.toAge(d.date);
      // 如果这个改变是来自birthday也就是timepicker的控件，那么现在要去改变的是后面岁数
      // 如果岁数没有变化就不变岁数，如果有变化就变岁数，如果单位没有变化就不变单位，如果单位有变化就变单位
      if (d.from === 'birthday') {
        // age是通过改变timepicker控件后得到的变化后的岁数
        // ageNum表示的原有的岁数
        if (age.age !== ageNum.value) {
          ageNum.patchValue(age.age, {emitEvent: false});
        }
        // ageUnit表示的是原有的单位
        if (age.unit !== ageUnit.value) {
          ageUnit.patchValue(age.unit, {emitEvent: false});
          // 如果单位有变化就把单位也变化了
          this.selectedUnit = age.unit;
        }
        // 组件通知view我这里有变化，view改变
        this.propagateChange(d.date);
      } else {
        // 当age变化时就会进入到这个语块
        const ageToCompare = this.toAge(birthday.value);
        if (age.age !== ageToCompare.age || age.unit !== ageToCompare.unit) {
          birthday.patchValue(d.date, {eventEmitter: false});
          this.propagateChange(d.date);
        }
      }
    });
  }


  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  private validate(c: FormControl): { [key: string]: any } {
    const val = c.value;
    if (!val) {
      return null;
    }
    if (isValidDate(val)) {
      return null;
    }
    return {
      ageInvalid: true
    };
  }

  private validateDate(c: FormControl): { [key: string]: any } {
    const val = c.value;
    return isValidDate(val) ? null : {
      birthdayInvalid: true
    };
  }

  private validateAge(ageNumKey: string, ageUnitKey: string) {
    return (group: FormGroup): { [key: string]: any } => {
      const ageNum = group.controls[ageNumKey];
      const ageUnit = group.controls[ageUnitKey];
      let result = false;
      const ageNumVal = ageNum.value;
      switch (ageUnit.value) {
        case AgeUnit.Year: {
          result = ageNumVal >= this.yearsBot && ageNumVal < this.yearsTop;
          break;
        }
        case AgeUnit.Month: {
          result = ageNumVal >= this.monthsBot && ageNumVal < this.monthsTop;
          break;
        }
        case AgeUnit.Day: {
          result = ageNumVal >= this.daysBot && ageNumVal < this.daysTop;
          break;
        }
        default: {
          break;
        }
      }
      return result ? null : {ageInvalid: true};
    };
  }

  private toAge(dateString: string): Age {
    const date = parse(dateString);
    const now = Date.now();
    // console.log(date);
    // console.log(sub_days(now, this.daysTop));
    // console.log(date);
    return is_before(sub_days(now, this.daysTop), date) ?
      {age: difference_in_days(now, date), unit: AgeUnit.Day} :
      is_before(sub_months(now, this.monthsTop), date) ?
        {age: difference_in_months(now, date), unit: AgeUnit.Month} :
        {
          age: difference_in_years(now, date),
          unit: AgeUnit.Year
        };
  }

  private toDate(age: Age): string {
    const now = Date.now();
    switch (age.unit) {
      case AgeUnit.Year: {
        return format(sub_years(now, age.age), this.format);
      }
      case AgeUnit.Month: {
        return format(sub_months(now, age.age), this.format);
      }
      case AgeUnit.Day: {
        return format(sub_days(now, age.age), this.format);
      }
      default: {
        return null;
      }
    }
  }
}
