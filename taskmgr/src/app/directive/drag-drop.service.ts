import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs/index';

@Injectable()
export class DragDropService {

  constructor() {
  }

  private _dragData = new BehaviorSubject<DragData>(null);

  getDragData(): Observable<DragData> {
    return this._dragData.asObservable();
  }

  setDragData(data: DragData) {
    this._dragData.next(data);
  }

  clearDragData() {
    this._dragData.next(null);
  }

}

export interface DragData {
  tag: string;
  data: any;
}
