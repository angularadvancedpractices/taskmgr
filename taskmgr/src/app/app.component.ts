import {Component, OnInit} from '@angular/core';
import {RootInjectChildService} from './root-inject-child.service';
import {OverlayContainer} from '@angular/cdk/overlay';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  darkTheme = false;

  switchTheme(dark) {
    this.darkTheme = dark;
    this.darkTheme ?
      this.overlayContainer.getContainerElement().classList.add('myapp-dark-theme') :
      this.overlayContainer.getContainerElement().classList.remove('myapp-dark-theme');
  }

  ngOnInit(): void {
  }

  constructor(private sharedService: RootInjectChildService,
              private overlayContainer: OverlayContainer) {
  }

}
