import {Project} from './project.model';
import {User} from './user.model';

export interface ProjectUsers {
  project: Project[];
  users: User[];
}
