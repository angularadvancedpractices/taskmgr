import {User} from './user.model';

export interface Task {
  _id?: string;
  taskListId: string;
  name: string;
  description: string;
  priority: number;
  owner: string;
  participants: any[];
  startDate: Date;
  dueDate: Date;
  // reminder: Date;
  createDate: Date;
  completed: boolean;
  remark: string;
  // taskListId: string;
}
