import {Task} from './task.model';

export interface TaskList {
  _id?: string;
  name: string;
  order: number;
  tasks?: Task[];
  projectId?: string;
}
