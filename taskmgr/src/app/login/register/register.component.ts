import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';
import {State} from '../../store/reducers/index';
import {Register} from "../../store/actions/auth.actions";
import {MatSnackBar} from "@angular/material";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  form: FormGroup;

  items: string[];
  private readonly avatarName = 'avatars';

  constructor(private fb: FormBuilder,
              private store$: Store<State>,
              private matSnackBar: MatSnackBar) {
  }

  ngOnInit() {
    const img = `${this.avatarName}:svg-${Math.floor(Math.random() * 16).toFixed(0)}`;
    const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16];
    this.items = numbers.map(d => `avatars:svg-${d}`);

    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      name: ['', Validators.required],
      password: ['', Validators.required],
      repeat: ['', Validators.required],
      dateOfBirth: ['1990-01-01'],
      avatar: [img],
    });
  }

  getErrorMessage() {
    return this.form.get('email').hasError('required') ? 'You must enter a value' :
      this.form.get('email').hasError('email') ? 'Not a valid email' :
        '';
  }

  onSubmit({value, valid}, event) {
    event.preventDefault();
    if (!valid) {
      return;
    } else if (value.password !== value.repeat) {
      this.matSnackBar.open('passwords do not match, try again later', 'dismiss', {duration: 3000});
      return;
    } else {
      this.store$.dispatch(new Register(value));
    }
  }
}
