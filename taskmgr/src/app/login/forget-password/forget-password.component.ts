import { Component, OnInit } from '@angular/core';
import {Store} from "@ngrx/store";
import * as fromRoot from '../../store/reducers';
import * as authActions from '../../store/actions/auth.actions';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss']
})
export class ForgetPasswordComponent implements OnInit {

  model: any = {};

  constructor(private store$: Store<fromRoot.State>) { }

  ngOnInit() {
  }

  onSubmit(ev: Event) {
    ev.preventDefault();
    // console.log(this.model.email)
    this.store$.dispatch(new authActions.ForgetPassword(this.model.email));
  }

}
