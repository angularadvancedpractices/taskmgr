import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {CheckIsLoginGuard} from '../services/check-is-login.guard';
import {ForgetPasswordComponent} from "./forget-password/forget-password.component";
import {ResetPasswordComponent} from "./reset-password/reset-password.component";

const routes: Routes = [
  {path: 'login', component: LoginComponent, canActivate: [CheckIsLoginGuard]},
  {path: 'register', component: RegisterComponent, canActivate: [CheckIsLoginGuard]},
  {path: 'forget-password', component: ForgetPasswordComponent, canActivate: [CheckIsLoginGuard]},
  {path: 'reset/:userId', component: ResetPasswordComponent, canActivate: [CheckIsLoginGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
