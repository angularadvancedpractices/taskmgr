import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Quote} from '../../domain/quote.model';
import {Observable} from 'rxjs/index';
import {Store} from '@ngrx/store';
import {getAuthState, getQuoteState, State} from '../../store/reducers/index';
import {Login} from '../../store/actions/auth.actions';
import {MatSnackBar} from '@angular/material';
import {ActivatedRoute, Router, RouterStateSnapshot} from "@angular/router";
import {PreviousUrlService} from "../../services/previous-url.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  getState$: Observable<any>;
  quote: Quote;

  constructor(private fb: FormBuilder,
              // private quoteService$: QuoteService,
              private store$: Store<State>,
              private matSnackBar: MatSnackBar,
              private route: ActivatedRoute) {
    this.getState$ = this.store$.select(getQuoteState);
    // this.store$.dispatch(new GetQuote(null));
  }

  ngOnInit() {
    this.route
      .queryParams
      .subscribe(params => {
        const p = params['from']
        if (p === 'register') {
          this.matSnackBar.open('You have successfully created an account, login now!', 'dismiss', {duration: 4000});
        } else if (p === 'reset') {
          this.matSnackBar.open('You have successfully reset your account, login now!', 'dismiss', {duration: 4000});
        } else if (p === 'forget') {
          this.matSnackBar.open('A reset email has been sent to your mailbox, please follow the link to reset your password', 'dismiss', {duration: 4000});
        }
      });
    const authState$ = this.store$.select(getAuthState);
    authState$.subscribe(auth => {
      if (auth.loginFail === true) {
        this.matSnackBar.open('email or password not correct', 'Dismiss', {
          duration: 2000,
          horizontalPosition: 'right',
          verticalPosition: 'top',
        });
      }
    });
    this.getState$.subscribe(state => {
      this.quote = state.quote;
    });
    // this.quoteService$.getQuote().subscribe(quote => {
    //   this.store$.dispatch(new GetQuoteSuccess(quote));
    // });
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  onSubmit({value, valid}, event) {
    event.preventDefault();
    if (!valid) {
      return;
    }
    this.store$.dispatch(new Login(value));
    // this.form.controls['email'].setValidators(this.validate);
  }

}
