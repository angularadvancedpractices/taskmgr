import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {MatSnackBar} from "@angular/material";
import * as fromRoot from '../../store/reducers';
import * as authActions from '../../store/actions/auth.actions';
import {Store} from "@ngrx/store";

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  userId: string;
  model: {userId: string, password: string, repeat: string} = {userId: '', password: '', repeat: ''};
  constructor(private route: ActivatedRoute,
              private matSnackBar: MatSnackBar,
              private store$: Store<fromRoot.State>) { }

  ngOnInit() {
    this.model.userId = this.route.snapshot.params['userId'];
  }

  onSubmit(ev: Event) {
    ev.preventDefault();
    if (this.model.password !== this.model.repeat) {
      this.matSnackBar.open('password do not match', 'dismiss', {duration: 3000});
    } else {
      this.store$.dispatch(new authActions.ResetPassword(this.model));
    }
  }

}
