import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Task} from '../domain/task.model';
import {count, map, mapTo, mergeMap, reduce, switchMap} from 'rxjs/internal/operators';
import {from, Observable} from 'rxjs/index';
import {TaskList} from '../domain/task-list.model';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  private readonly domain = 'tasks';
  private headers = new HttpHeaders({
    'content-type': 'application/json'
  });

  constructor(private http: HttpClient, @Inject('BASE_CONFIG') private config) {
  }

  add(task: Task, taskListId: string): Observable<Task> {
    const uri = `${this.config.uri}/${this.domain}`;
    return this.http
      .post<Task>(uri, {task: task, taskListId: taskListId}, {headers: this.headers})
      .pipe(map(res => res));
  }

  get(taskListId: string): Observable<Task[]> {
    const uri = `${this.config.uri}/${this.domain}/taskLists/${taskListId}`;
    return this.http
      .get<Task[]>(uri)
      .pipe(map(res => res));
  }

  getAll(): Observable<Task[]> {
    const uri = `${this.config.uri}/${this.domain}`;
    return this.http
      .get<Task[]>(uri)
      .pipe(map(res => res));
  }

  update(task): Observable<Task> {
    const uri = `${this.config.uri}/${this.domain}/${task._id}`;
    const toUpdate = {
      name: task.name,
      description: task.description,
      priority: task.priority,
      startDate: task.startDate,
      dueDate: task.dueDate,
      owner: task.ownerId,
      participants: task.participantIds,
      remark: task.remark,
    };
    // 使用patch可以选择性更新
    return this.http
      .post<Task>(uri, {task: toUpdate}, {headers: this.headers})
      .pipe(map(res => res));
  }

  del(task: Task): Observable<Task> {
    const uri = `${this.config.uri}/${this.domain}/${task._id}`;
    return this.http.delete(uri)
      .pipe(mapTo(task));
  }

  getByLists(lists: TaskList[]): Observable<Task[]> {
    return from(lists)
      .pipe(
        mergeMap(list => this.get(list._id)),
        reduce((tasks: Task[], t: Task[]) => [...tasks, ...t], []));
  }

  complete(task: Task): Observable<Task> {
    const uri = `${this.config.uri}/${this.domain}/complete/${task._id}`;
    return this.http
      .post<Task>(uri, {completed: !task.completed}, {headers: this.headers})
      .pipe(map(res => res));
  }

  move(task: Task, targetListId: string): Observable<any> {
    const uri = `${this.config.uri}/${this.domain}/move`;
    // 使用patch可以选择性更新
    return this.http
      .post<{
        updatedTask: Task,
        taskListRemovedTaskId: TaskList,
        taskListAddedTaskId: TaskList
      }>(uri, {task: task, targetListId: targetListId}, {headers: this.headers})
      .pipe(map(res => res));
  }

  moveAll(srcListId: string, targetListId: string): Observable<{ tasks: Task[], srcListId: string, targetListId: string }> {
    const uri = `${this.config.uri}/${this.domain}/moveAll`;
    return this.http
      .post<{ tasks: Task[], srcListId: string, targetListId: string }>(uri, {
        srcListId: srcListId,
        targetListId: targetListId
      }, {headers: this.headers})
      .pipe(map(res => res));
  }
}
