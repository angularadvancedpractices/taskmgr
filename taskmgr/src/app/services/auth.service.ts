import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {User} from '../domain/user.model';
import {Observable} from 'rxjs/index';
import {Auth} from '../domain/auth.model';
import {switchMap, map, catchError, tap} from 'rxjs/internal/operators';
import {localStorageSync} from 'ngrx-store-localstorage';

// for login and register
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly domain = 'users';
  private headers = new HttpHeaders({
    'content-type': 'application/json'
  });
  isLoggedIn = false;
  // JWT token
  // private token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9' +
  //   '.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9' +
  //   '.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ';

  constructor(private http: HttpClient, @Inject('BASE_CONFIG') private config) {
  }

  // todo: after fix the server side registration, come back
  register(user: User): Observable<Auth> {
    const uri = `${this.config.uri}/auth/register`;
    return this.http
      .post<Auth>(uri, {user}, {headers: this.headers})
      .pipe(
        map(r => r)
      )
  }

  login(username: string, password: string): Observable<Auth> {
    const uri = `${this.config.uri}/auth/login`;
    return this.http
      .post<any>(uri, {params: {email: username, password: password}}, {
        headers: new HttpHeaders({
          'content-type': 'application/json'
        })
      })
      .pipe(
        tap(_ => this.isLoggedIn = true),
        map(res => {
          if (res.token && res.user) {
            localStorage.setItem('currentUserToken', JSON.stringify(res.token));
            return {
              token: res.token,
              user: {
                _id: res.user._id,
                email: res.user.email,
                name: res.user.name,
                avatar: res.user.avatar,
                projects: res.user.projectIds
              }
            };
          }
        }));
  }

  forgetPassword(email: string): any {
    const uri = `${this.config.uri}/auth/forget`;
    return this.http
      .post(uri, {email}, {headers: this.headers})
      .pipe(
        map(r => r)
      )
  }

  resetPassword(userModel: {userId: string, password: string, repeat: string}): any {
    const uri = `${this.config.uri}/auth/reset`;
    return this.http
      .post(uri, {userModel}, {headers: this.headers})
      .pipe(
        map(r => r)
      )
  }

  logout(): void {
    this.isLoggedIn = false;
    localStorage.removeItem('currentUserToken');
    localStorage.removeItem('root');
  }
}
