import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {from, Observable, of} from 'rxjs/index';
import {User} from '../domain/user.model';
import {Project} from '../domain/project.model';
import {filter, reduce, switchMap} from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly domain = 'users';
  private headers = new HttpHeaders({
    'content-type': 'application/json'
  });

  constructor(private http: HttpClient, @Inject('BASE_CONFIG') private config) {
  }

  searchUsers(f: string): Observable<User[]> {
    const uri = `${this.config.uri}/${this.domain}/search`;
    return this.http
      .get<User[]>(uri, {params: {keyword: f}});
  }

  getUsersByProjectId(projectId: string): Observable<User[]> {
    const uri = `${this.config.uri}/${this.domain}/project/id`;
    return this.http
      .get<User[]>(uri, {params: {projectId: projectId}});
  }

  addProjectRef(user: User, projectId: string): Observable<any> {
    const uri = `${this.config.uri}/${this.domain}`;
    const projectIds = user.projects ? user.projects : [];
    if (projectIds.indexOf(projectId) > -1) {
      return of(user);
    }
    return this.http
      .patch(uri, JSON.stringify({projectIds: [...projectIds, projectId]}), {headers: this.headers});
  }

  removeProjectRef(user: User, projectId: string): Observable<User> {
    const uri = `${this.config.uri}/${this.domain}`;
    const projectIds = user.projects ? user.projects : [];
    const index = projectIds.indexOf(projectId);
    if (index === -1) {
      return of(user);
    }
    // const toUpdate = projectIds.splice(index, 1);
    const toUpdate = [...projectIds.slice(0, index), ...projectIds.slice(index + 1)];
    return this.http
      .patch<User>(uri, JSON.stringify({projectIds: [...projectIds, toUpdate]}), {headers: this.headers});
  }

  batchUpdateProjectRef(project: Project): Observable<User[]> {
    const projectId = project._id;
    const memberIds = project.members ? project.members : [];
    return from(memberIds)
      .pipe(
        switchMap(id => {
          const uri = `${this.config.uri}/${this.domain}/${id}`;
          return this.http.get<User>(uri);
        }),
        filter(user => user.projects.indexOf(projectId) === -1),
        switchMap(u => this.addProjectRef(u, projectId)),
        reduce((users, curr) => [...users, curr], []));
  }
}
