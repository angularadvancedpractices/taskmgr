import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/index';
import {Quote} from '../domain/quote.model';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/internal/operators';
import {debug} from '../utils/debug.util';

@Injectable({
  providedIn: 'root'
})
export class QuoteService {

  constructor(@Inject('BASE_CONFIG') private config,
              private httpClient: HttpClient) {
  }

  getQuote(): Observable<Quote> {
    const uri = `${this.config.uri}/quotes/${Math.floor(Math.random() * 10)}`;
    return this.httpClient.get<Quote>(uri);
  }
}
