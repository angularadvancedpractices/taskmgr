import {Inject, Injectable} from '@angular/core';
import {Project} from '../domain';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {count, map, mapTo, mergeMap, switchMap, tap} from 'rxjs/internal/operators';
import {from, Observable} from 'rxjs/index';
import {User} from '../domain/user.model';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  private readonly domain = 'projects';
  private headers = new HttpHeaders({
    'content-type': 'application/json'
  });

  constructor(private http: HttpClient, @Inject('BASE_CONFIG') private config) {
  }

  add(project: Project): Observable<Project> {
    const uri = `${this.config.uri}/${this.domain}`;
    return this.http
      .post<Project>(uri, project, {headers: this.headers});
  }

  update(project: Project): Observable<Project> {
    const uri = `${this.config.uri}/${this.domain}/${project._id}`;
    console.log(project);
    const toUpdate = {
      name: project.name,
      desc: project.desc,
      coverImg: project.coverImg
    };
    // 使用patch可以选择性更新
    return this.http
      .post<Project>(uri, JSON.stringify(toUpdate), {headers: this.headers});
  }

  del(project: Project): Observable<Project> {
    const uri = `${this.config.uri}/${this.domain}/${project._id}`;
    return this.http
      .delete<Project>(uri);
  }

  getProjectsByUserId(userId: string): Observable<Project[]> {
    const uri = `${this.config.uri}/${this.domain}/user/id`;
    return this.http
      .get<Project[]>(uri, {params: {userId: userId}});
  }

  deleteProjectMembers(projectMemberIds: string[]): Observable<Project> {
    const uri = `${this.config.uri}/${this.domain}/deleteProjectMembers`;
    return this.http
      .post<Project>(uri, {projectMemberIds: projectMemberIds});
  }

  invite(projectId: string, inviteUsers: User[]): Observable<Project> {
    const uri = `${this.config.uri}/${this.domain}/update-members`;
    return this.http
      .post<Project>(uri, {projectId, inviteUsers});
  }
}
