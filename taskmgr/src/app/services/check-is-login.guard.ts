import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Observable, of} from 'rxjs';
import {Store} from "@ngrx/store";
import {getAuthState, State} from "../store/reducers/index";
import {PreviousUrlService} from "./previous-url.service";
import {Auth} from "../domain/auth.model";
import {map} from "rxjs/internal/operators";
import {AuthService} from "./auth.service";

@Injectable({
  providedIn: 'root'
})
export class CheckIsLoginGuard implements CanActivate {
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      // 如果已经登陆
      if (localStorage.getItem('currentUserToken')) {
        this.router.navigate(['/project']);
        return false;
      } else {
        // 如果未登陆
        return true;
      }
    }

  constructor (private router: Router) {
  }

}
