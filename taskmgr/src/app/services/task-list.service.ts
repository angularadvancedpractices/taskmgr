import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {concat, from, Observable} from 'rxjs/index';
import {count, map, mapTo, mergeMap, reduce, switchMap} from 'rxjs/internal/operators';
import {TaskList} from '../domain/task-list.model';

@Injectable({
  providedIn: 'root'
})
export class TaskListService {
  private readonly domain = 'task-lists';
  private headers = new HttpHeaders({
    'content-type': 'application/json'
  });

  constructor(private http: HttpClient, @Inject('BASE_CONFIG') private config) {
  }

  add(taskList: TaskList): Observable<TaskList> {
    const uri = `${this.config.uri}/${this.domain}`;
    return this.http
      .post<TaskList>(uri, JSON.stringify(taskList), {headers: this.headers})
      .pipe(map(res => res));
  }

  update(taskList: TaskList): Observable<TaskList> {
    const uri = `${this.config.uri}/${this.domain}/${taskList._id}`;
    return this.http
      .post<TaskList>(uri, JSON.stringify(taskList), {headers: this.headers})
      .pipe(map(res => res));
  }

  del(taskList: TaskList, projectId: string): Observable<TaskList> {
    const uri = `${this.config.uri}/${this.domain}/${taskList._id}/projectId/${projectId}`;
    return this.http.delete(uri)
      .pipe(mapTo(taskList));
  }

  get(projectId: string): Observable<TaskList[]> {
    const uri = `${this.config.uri}/${this.domain}/${projectId}`;
    return this.http
      .get<TaskList[]>(uri)
      .pipe(map(res => res as TaskList[]));
  }

  getAll(): Observable<TaskList[]> {
    const uri = `${this.config.uri}/${this.domain}`;
    return this.http
      .get<TaskList[]>(uri)
      .pipe(map(res => res as TaskList[]));
  }

  swapOrder(src: TaskList, target: TaskList): Observable<TaskList[]> {
    const uri = `${this.config.uri}/${this.domain}/swap`;
    return this.http
      .post<TaskList[]>(uri, {src, target})
      .pipe(map(res => res as TaskList[]));
  }
}
