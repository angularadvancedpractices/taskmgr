import {Action} from '@ngrx/store';
import {User} from '../../domain/user.model';
import {Auth} from "../../domain/auth.model";

export enum AuthActionTypes {
  LOGIN = '[Login] Login',
  LOGIN_SUCCESS = '[Login Success] Login Success',
  LOGIN_FAILURE = '[Login Failure] Login Failure',
  REGISTER = '[Register] Register',
  REGISTER_SUCCESS = '[Register Success] Register Success',
  REGISTER_FAILURE = '[Register Failure] Register Failure',
  FORGET = '[Forget Password] Forget',
  FORGET_SUCCESS = '[Forget Password] Successful',
  FORGET_FAIL = '[Forget Password] Fail',
  RESET = '[Reset Password] Reset',
  RESET_SUCCESS = '[Reset Password] Successful',
  RESET_FAIL = '[Reset Password] Fail',
  LOGOUT = '[Logout] Logout'
}

export class Login implements Action {
  readonly type = AuthActionTypes.LOGIN;
  constructor(public payload: {email: string; password: string}) {
  }
}

export class LoginSuccess implements Action {
  readonly type = AuthActionTypes.LOGIN_SUCCESS;
  constructor(public payload: Auth) {
  }
}

export class LoginFailure implements Action {
  readonly type = AuthActionTypes.LOGIN_FAILURE;
  constructor(public payload: string) {
  }
}

export class Register implements Action {
  readonly type = AuthActionTypes.REGISTER;
  constructor(public payload: User) {
  }
}

export class RegisterSuccess implements Action {
  readonly type = AuthActionTypes.REGISTER_SUCCESS;
  constructor(public payload: Auth) {
  }
}

export class RegisterFailure implements Action {
  readonly type = AuthActionTypes.REGISTER_FAILURE;
  constructor(public payload: string) {
  }
}

export class ResetPassword implements Action {
  readonly type = AuthActionTypes.RESET;
  constructor(public payload: {userId: string, password: string, repeat: string}) {
  }
}

export class ResetPasswordSuccess implements Action {
  readonly type = AuthActionTypes.RESET_SUCCESS;
  constructor(public payload: any) {
  }
}

export class ResetPasswordFail implements Action {
  readonly type = AuthActionTypes.RESET_FAIL;
  constructor(public payload: any) {
  }
}

export class ForgetPassword implements Action {
  readonly type = AuthActionTypes.FORGET;
  constructor(public payload: string) {
  }
}

export class ForgetPasswordSuccess implements Action {
  readonly type = AuthActionTypes.FORGET_SUCCESS;
  constructor(public payload: any) {
  }
}

export class ForgetPasswordFail implements Action {
  readonly type = AuthActionTypes.FORGET_FAIL;
  constructor(public payload: any) {
  }
}


export class Logout implements Action {
  readonly type = AuthActionTypes.LOGOUT;
  constructor(public payload) {
  }
}

export type AuthActions =
  | Login
  | LoginSuccess
  | LoginFailure
  | Register
  | RegisterSuccess
  | RegisterFailure
  | ForgetPassword
  | ForgetPasswordSuccess
  | ForgetPasswordFail
  | ResetPassword
  | ResetPasswordSuccess
  | ResetPasswordFail
  | Logout;
