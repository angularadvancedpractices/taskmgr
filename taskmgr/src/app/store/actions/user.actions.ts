import {Action} from '@ngrx/store';
import {User} from '../../domain/user.model';
import {Project} from '../../domain/project.model';

export interface UserProject {
  user: User;
  projectId: string;
}

export enum UserActionTypes {
  ADD = '[User] Add',
  ADD_SUCCESS = '[User] Add Success',
  ADD_FAILURE = '[User] Add Failure',
  UPDATE = '[User] Update',
  UPDATE_SUCCESS = '[User] Update Success',
  UPDATE_FAILURE = '[User] Update Failure',
  DELETE = '[User] Delete',
  DELETE_SUCCESS = '[User] Delete Success',
  DELETE_FAILURE = '[User] Delete Failure',
  LOAD = '[User] Load',
  LOAD_SUCCESS = '[User] Load Success',
  LOAD_FAILURE = '[User] Load Failure',
  SEARCH = '[User] Search',
  SEARCH_SUCCESS = '[User] Search Success',
  SEARCH_FAILURE = '[User] Search Failure',
  INVITE_PROJECT_USER = '[User] Invite Project Success',
}

export class AddUser implements Action {
  public readonly type = UserActionTypes.ADD;

  constructor(public payload: UserProject) {
  }
}

export class AddUserSuccess implements Action {
  public readonly type = UserActionTypes.ADD_SUCCESS;

  constructor(public payload: User) {
  }
}

export class AddUserFailure implements Action {
  readonly type = UserActionTypes.ADD_FAILURE;

  constructor(public payload: string) {
  }
}

// update here is batch update
export class UpdateUser implements Action {
  readonly type = UserActionTypes.UPDATE;

  constructor(public payload: Project) {
  }
}

export class UpdateUserSuccess implements Action {
  readonly type = UserActionTypes.UPDATE_SUCCESS;

  constructor(public payload: User[]) {
  }
}

export class UpdateUserFailure implements Action {
  readonly type = UserActionTypes.UPDATE_FAILURE;

  constructor(public payload: string) {
  }
}

// delete here is to delete the reference between the user and project, not real deleting the user
export class DeleteUser implements Action {
  readonly type = UserActionTypes.DELETE;

  constructor(public payload: UserProject) {
  }
}

export class DeleteUserSuccess implements Action {
  readonly type = UserActionTypes.DELETE_SUCCESS;

  constructor(public payload: User) {
  }
}

export class DeleteUserFailure implements Action {
  readonly type = UserActionTypes.DELETE_FAILURE;

  constructor(public payload: string) {
  }
}

export class LoadUser implements Action {
  readonly type = UserActionTypes.LOAD;

  // use project id to load user
  constructor(public payload: string) {
  }
}

export class LoadUserSuccess implements Action {
  readonly type = UserActionTypes.LOAD_SUCCESS;

  constructor(public payload: User[]) {
  }
}

export class LoadUserFailure implements Action {
  readonly type = UserActionTypes.LOAD_FAILURE;

  constructor(public payload: string) {
  }
}

export class SearchUser implements Action {
  readonly type = UserActionTypes.SEARCH;

  // payload is a keyword used for searching user
  constructor(public payload: string) {
  }
}

export class SearchUserSuccess implements Action {
  readonly type = UserActionTypes.SEARCH_SUCCESS;

  constructor(public payload: User[]) {
  }
}

export class SearchUserFailure implements Action {
  readonly type = UserActionTypes.SEARCH_FAILURE;

  constructor(public payload: string) {
  }
}

// export class InviteProjectUser implements Action {
//   readonly type = UserActionTypes.INVITE_PROJECT_USER;
//
//   constructor(public payload: User[]) {
//   }
// }

export type UserActions =
  | AddUser
  | AddUserSuccess
  | AddUserFailure
  | UpdateUser
  | UpdateUserSuccess
  | UpdateUserFailure
  | DeleteUser
  | DeleteUserSuccess
  | DeleteUserFailure
  | LoadUser
  | LoadUserSuccess
  | LoadUserFailure
  | SearchUser
  | SearchUserSuccess
  | SearchUserFailure;
