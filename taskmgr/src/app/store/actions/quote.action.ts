import {Action} from '@ngrx/store';
import {Quote} from '../../domain/quote.model';

export enum QuoteActionTypes {
  QUOTE = '[Quote] Quote',
  QUOTE_SUCCESS = '[Quote Success] Quote Success',
  QUOTE_FAILURE = '[Quote Failure] Quote Failure'
}

export class GetQuote implements Action {
  readonly type = QuoteActionTypes.QUOTE;
  constructor(public payload: Quote) {
  }
}

export class GetQuoteSuccess implements Action {
  readonly type = QuoteActionTypes.QUOTE_SUCCESS;
  constructor(public payload: Quote) {
  }
}

export class GetQuoteFailure implements Action {
  readonly type = QuoteActionTypes.QUOTE_FAILURE;
  constructor(public payload: Quote) {
  }
}

export type QuoteActions =
  | GetQuote
  | GetQuoteSuccess
  | GetQuoteFailure;
