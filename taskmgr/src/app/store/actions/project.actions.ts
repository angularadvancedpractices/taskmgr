import {Action} from '@ngrx/store';
import {Project} from '../../domain/project.model';
import {User} from '../../domain/user.model';

export enum ProjectActionTypes {
  ADD = '[Project] Add',
  ADD_SUCCESS = '[Project] Add Success',
  ADD_FAILURE = '[Project] Add Failure',
  UPDATE = '[Project] Update',
  UPDATE_SUCCESS = '[Project] Update Success',
  UPDATE_FAILURE = '[Project] Update Failure',
  DELETE = '[Project] Delete',
  DELETE_SUCCESS = '[Project] Delete Success',
  DELETE_FAILURE = '[Project] Delete Failure',
  LOAD = '[Project] Load',
  LOAD_SUCCESS = '[Project] Load Success',
  LOAD_FAILURE = '[Project] Load Failure',
  INVITE = '[Project] Invite',
  INVITE_SUCCESS = '[Project] Invite Success',
  INVITE_FAILURE = '[Project] Invite Failure',
  SELECT_PROJECT = '[Project] Select'
}

export class AddProject implements Action {
  readonly type = ProjectActionTypes.ADD;
  constructor(public payload: Project) {
  }
}

export class AddProjectSuccess implements Action {
  readonly type = ProjectActionTypes.ADD_SUCCESS;
  constructor(public payload: Project) {
  }
}

export class AddProjectFailure implements Action {
  readonly type = ProjectActionTypes.ADD_FAILURE;
  constructor(public payload: string) {
  }
}

export class UpdateProject implements Action {
  readonly type = ProjectActionTypes.UPDATE;
  constructor(public payload: Project) {
  }
}

export class UpdateProjectSuccess implements Action {
  readonly type = ProjectActionTypes.UPDATE_SUCCESS;
  constructor(public payload: any) {
  }
}

export class UpdateProjectFailure implements Action {
  readonly type = ProjectActionTypes.UPDATE_FAILURE;
  constructor(public payload: string) {
  }
}

export class DeleteProject implements Action {
  readonly type = ProjectActionTypes.DELETE;
  constructor(public payload: Project) {
  }
}

export class DeleteProjectSuccess implements Action {
  readonly type = ProjectActionTypes.DELETE_SUCCESS;
  constructor(public payload: Project) {
  }
}

export class DeleteProjectFailure implements Action {
  readonly type = ProjectActionTypes.DELETE_FAILURE;
  constructor(public payload: string) {
  }
}

export class LoadProject implements Action {
  readonly type = ProjectActionTypes.LOAD;
  constructor(public payload: any) {
  }
}

export class LoadProjectSuccess implements Action {
  readonly type = ProjectActionTypes.LOAD_SUCCESS;
  constructor(public payload: Project[]) {
  }
}

export class LoadProjectFailure implements Action {
  readonly type = ProjectActionTypes.LOAD_FAILURE;
  constructor(public payload: string) {
  }
}

export class InviteProject implements Action {
  readonly type = ProjectActionTypes.INVITE;
  constructor(public payload: {projectId: string, members: User[]}) {
  }
}

export class InviteProjectSuccess implements Action {
  readonly type = ProjectActionTypes.INVITE_SUCCESS;
  constructor(public payload: Project) {
  }
}

export class InviteProjectFailure implements Action {
  readonly type = ProjectActionTypes.INVITE_FAILURE;
  constructor(public payload: string) {
  }
}

export class SelectProject implements Action {
  readonly type = ProjectActionTypes.SELECT_PROJECT;
  constructor(public payload: Project) {
  }
}

export type ProjectActions =
  | AddProject
  | AddProjectSuccess
  | AddProjectFailure
  | UpdateProject
  | UpdateProjectSuccess
  | UpdateProjectFailure
  | DeleteProject
  | DeleteProjectSuccess
  | DeleteProjectFailure
  | LoadProject
  | LoadProjectSuccess
  | LoadProjectFailure
  | InviteProject
  | InviteProjectSuccess
  | InviteProjectFailure
  | SelectProject;
