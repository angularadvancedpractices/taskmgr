import {Action} from '@ngrx/store';
import {TaskList} from '../../domain/task-list.model';
import {User} from '../../domain/user.model';

export enum TaskListActionTypes {
  ADD = '[TaskList] Add',
  ADD_SUCCESS = '[TaskList] Add Success',
  ADD_FAILURE = '[TaskList] Add Failure',
  UPDATE = '[TaskList] Update',
  UPDATE_SUCCESS = '[TaskList] Update Success',
  UPDATE_FAILURE = '[TaskList] Update Failure',
  DELETE = '[TaskList] Delete',
  DELETE_SUCCESS = '[TaskList] Delete Success',
  DELETE_FAILURE = '[TaskList] Delete Failure',
  LOAD = '[TaskList] Load',
  LOAD_SUCCESS = '[TaskList] Load Success',
  LOAD_FAILURE = '[TaskList] Load Failure',
  LOAD_ALL = '[TaskList] Load All',
  LOAD_ALL_SUCCESS = '[TaskList] Load All Success',
  LOAD_ALL_FAILURE = '[TaskList] Load All Failure',
  SWAP = '[TaskList] Swap',
  SWAP_SUCCESS = '[TaskList] Swap Success',
  SWAP_FAILURE = '[TaskList] Swap Failure',
}

export class AddTaskList implements Action {
  public readonly type = TaskListActionTypes.ADD;
  constructor(public payload: TaskList) {
  }
}

export class AddTaskListSuccess implements Action {
  public readonly type = TaskListActionTypes.ADD_SUCCESS;
  constructor(public payload: TaskList) {}
}

export class AddTaskListFailure implements Action {
  readonly type = TaskListActionTypes.ADD_FAILURE;
  constructor(public payload: string) {
  }
}

export class UpdateTaskList implements Action {
  readonly type = TaskListActionTypes.UPDATE;
  constructor(public payload: TaskList) {
  }
}

export class UpdateTaskListSuccess implements Action {
  readonly type = TaskListActionTypes.UPDATE_SUCCESS;
  constructor(public payload: any) {
  }
}

export class UpdateTaskListFailure implements Action {
  readonly type = TaskListActionTypes.UPDATE_FAILURE;
  constructor(public payload: string) {
  }
}

export class DeleteTaskList implements Action {
  readonly type = TaskListActionTypes.DELETE;
  constructor(public payload: {taskList: TaskList, projectId: string}) {
  }
}

export class DeleteTaskListSuccess implements Action {
  readonly type = TaskListActionTypes.DELETE_SUCCESS;
  constructor(public payload: any) {
  }
}

export class DeleteTaskListFailure implements Action {
  readonly type = TaskListActionTypes.DELETE_FAILURE;
  constructor(public payload: string) {
  }
}

export class LoadTaskLists implements Action {
  readonly type = TaskListActionTypes.LOAD;
  constructor(public payload: string) {
  }
}

export class LoadTaskListSuccess implements Action {
  readonly type = TaskListActionTypes.LOAD_SUCCESS;
  constructor(public payload: TaskList[]) {
  }
}

export class LoadTaskListFailure implements Action {
  readonly type = TaskListActionTypes.LOAD_FAILURE;
  constructor(public payload: string) {
  }
}

export class LoadAllTaskLists implements Action {
  readonly type = TaskListActionTypes.LOAD_ALL;
  constructor(public payload: any) {
  }
}

export class LoadAllTaskListSuccess implements Action {
  readonly type = TaskListActionTypes.LOAD_ALL_SUCCESS;
  constructor(public payload: TaskList[]) {
  }
}

export class LoadAllTaskListFailure implements Action {
  readonly type = TaskListActionTypes.LOAD_ALL_FAILURE;
  constructor(public payload: string) {
  }
}


export class SwapTaskList implements Action {
  readonly type = TaskListActionTypes.SWAP;
  constructor(public payload: TaskList[]) {
  }
}

export class SwapTaskListSuccess implements Action {
  readonly type = TaskListActionTypes.SWAP_SUCCESS;
  constructor(public payload: TaskList[]) {
  }
}

export class SwapTaskListFailure implements Action {
  readonly type = TaskListActionTypes.SWAP_FAILURE;
  constructor(public payload: string) {
  }
}

export type TaskListActions =
  | AddTaskList
  | AddTaskListSuccess
  | AddTaskListFailure
  | UpdateTaskList
  | UpdateTaskListSuccess
  | UpdateTaskListFailure
  | DeleteTaskList
  | DeleteTaskListSuccess
  | DeleteTaskListFailure
  | LoadTaskLists
  | LoadTaskListSuccess
  | LoadTaskListFailure
  | LoadAllTaskLists
  | LoadAllTaskListSuccess
  | LoadAllTaskListFailure
  | SwapTaskList
  | SwapTaskListSuccess
  | SwapTaskListFailure;
