import {Action} from '@ngrx/store';
import {Task} from '../../domain/task.model';
import {User} from '../../domain/user.model';
import {TaskList} from "../../domain/task-list.model";

export enum TaskActionTypes {
  ADD = '[Task] Add',
  ADD_SUCCESS = '[Task] Add Success',
  ADD_FAILURE = '[Task] Add Failure',
  UPDATE = '[Task] Update',
  UPDATE_SUCCESS = '[Task] Update Success',
  UPDATE_FAILURE = '[Task] Update Failure',
  DELETE = '[Task] Delete',
  DELETE_SUCCESS = '[Task] Delete Success',
  DELETE_FAILURE = '[Task] Delete Failure',
  LOAD = '[Task] Load',
  LOAD_SUCCESS = '[Task] Load Success',
  LOAD_FAILURE = '[Task] Load Failure',
  LOAD_ALL_TASKS = '[Task] Load All',
  LOAD_ALL_TASKS_SUCCESS = '[Task] Load All Success',
  LOAD_ALL_TASKS_FAILURE = '[Task] Load All Failure',
  MOVE_ALL = '[Task] Move all',
  MOVE_ALL_SUCCESS = '[Task] Move all success',
  MOVE_ALL_FAILURE = '[Task] Move all Failure',
  MOVE = '[Task] Move',
  MOVE_SUCCESS = '[Task] Move Success',
  MOVE_FAILURE = '[Task] Move Failure',
  COMPLETE = '[Task] Complete',
  COMPLETE_SUCCESS = '[Task] Complete Success',
  COMPLETE_FAILURE = '[Task] Complete Failure'
}

export class AddTask implements Action {
  public readonly type = TaskActionTypes.ADD;

  constructor(public payload: { task: Task, taskListId: string }) {
  }
}

export class AddTaskSuccess implements Action {
  public readonly type = TaskActionTypes.ADD_SUCCESS;

  constructor(public payload: Task) {
  }
}

export class AddTaskFailure implements Action {
  readonly type = TaskActionTypes.ADD_FAILURE;

  constructor(public payload: string) {
  }
}

export class UpdateTask implements Action {
  readonly type = TaskActionTypes.UPDATE;

  constructor(public payload: Task) {
  }
}

export class UpdateTaskSuccess implements Action {
  readonly type = TaskActionTypes.UPDATE_SUCCESS;

  constructor(public payload: any) {
  }
}

export class UpdateTaskFailure implements Action {
  readonly type = TaskActionTypes.UPDATE_FAILURE;

  constructor(public payload: string) {
  }
}

export class DeleteTask implements Action {
  readonly type = TaskActionTypes.DELETE;

  constructor(public payload: Task) {
  }
}

export class DeleteTaskSuccess implements Action {
  readonly type = TaskActionTypes.DELETE_SUCCESS;

  constructor(public payload: any) {
  }
}

export class DeleteTaskFailure implements Action {
  readonly type = TaskActionTypes.DELETE_FAILURE;

  constructor(public payload: string) {
  }
}

export class LoadTask implements Action {
  readonly type = TaskActionTypes.LOAD;

  constructor(public payload: string) {
  }
}

export class LoadTaskSuccess implements Action {
  readonly type = TaskActionTypes.LOAD_SUCCESS;

  constructor(public payload: Task[]) {
  }
}

export class LoadTaskFailure implements Action {
  readonly type = TaskActionTypes.LOAD_FAILURE;

  constructor(public payload: string) {
  }
}

export class LoadAllTasks implements Action {
  readonly type = TaskActionTypes.LOAD_ALL_TASKS;

  constructor(public payload: any) {
  }
}

export class LoadAllTasksSuccess implements Action {
  readonly type = TaskActionTypes.LOAD_ALL_TASKS_SUCCESS;

  constructor(public payload: Task[]) {
  }
}

export class LoadAllTasksFailure implements Action {
  readonly type = TaskActionTypes.LOAD_ALL_TASKS_FAILURE;

  constructor(public payload: string) {
  }
}

export class MoveAll implements Action {
  readonly type = TaskActionTypes.MOVE_ALL;

  constructor(public payload: { srcListId: string; targetListId: string }) {
  }
}

export class MoveAllSuccess implements Action {
  readonly type = TaskActionTypes.MOVE_ALL_SUCCESS;

  constructor(public payload: {tasks: Task[], srcListId: string, targetListId: string}) {
  }
}

export class MoveAllFailure implements Action {
  readonly type = TaskActionTypes.MOVE_ALL_FAILURE;

  constructor(public payload: string) {
  }
}

export class MoveTask implements Action {
  readonly type = TaskActionTypes.MOVE;

  constructor(public payload: { task: Task; targetListId: string }) {
  }
}

export class MoveTaskSuccess implements Action {
  readonly type = TaskActionTypes.MOVE_SUCCESS;

  constructor(public payload: {
    updatedTask: Task,
    taskListRemovedTaskId: TaskList,
    taskListAddedTaskId: TaskList
  }) {
  }
}

export class MoveTaskFailure implements Action {
  readonly type = TaskActionTypes.MOVE_FAILURE;

  constructor(public payload: string) {
  }
}

export class CompleteTask implements Action {
  readonly type = TaskActionTypes.COMPLETE;

  constructor(public payload: Task) {
  }
}

export class CompleteTaskSuccess implements Action {
  readonly type = TaskActionTypes.COMPLETE_SUCCESS;

  constructor(public payload: Task) {
  }
}

export class CompleteTaskFailure implements Action {
  readonly type = TaskActionTypes.COMPLETE_FAILURE;

  constructor(public payload: string) {
  }
}

export type TaskActions =
  | AddTask
  | AddTaskSuccess
  | AddTaskFailure
  | UpdateTask
  | UpdateTaskSuccess
  | UpdateTaskFailure
  | DeleteTask
  | DeleteTaskSuccess
  | DeleteTaskFailure
  | LoadTask
  | LoadTaskSuccess
  | LoadTaskFailure
  | LoadAllTasks
  | LoadAllTasksSuccess
  | LoadAllTasksFailure
  | MoveAll
  | MoveAllSuccess
  | MoveAllFailure
  | MoveTask
  | MoveTaskSuccess
  | MoveTaskFailure
  | CompleteTask
  | CompleteTaskSuccess
  | CompleteTaskFailure;
