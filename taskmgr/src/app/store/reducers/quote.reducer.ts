import {Quote} from '../../domain/quote.model';
import * as actions from '../actions/quote.action';

export interface State {
  quote: Quote;
}

export const initialState: State = {
  quote: {
    id: 0,
    content: 'I suddenly feel myself like a doll,acting all kinds of joys and sorrows.' +
    'There are lots of shining silvery thread on my back,controlling all my action.',
    pic: '/assets/img/quotes/0.jpg'
  }
};

export function reducer(state: State = initialState, action: actions.QuoteActions): State {
  switch (action.type) {
    case actions.QuoteActionTypes.QUOTE_SUCCESS:
      return {...state, quote: action.payload};
    case actions.QuoteActionTypes.QUOTE_FAILURE:
    default:
      return state;
  }
}
