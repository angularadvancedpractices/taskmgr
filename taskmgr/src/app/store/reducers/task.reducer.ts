import * as taskAction from '../actions/task.actions';
import * as projectAction from '../actions/project.actions';
import * as _ from 'lodash';
import {Task} from '../../domain/task.model';

// basically all the logical things should be done in the backend and frontend should be only receiving the results
// for display, but the mock data only serves as a database, so the logic things need to be done in the frontend and
// then show on the page

/*
ids: all tasks' id been stored in the state
entities: all tasks are been stored in an object consisting of project with id as the key. Using dictionary makes
tasklist edit/update/delete easier
 */
export interface TaskState {
  ids: string[];
  entities: { [id: string]: Task };
}

export const initialState: TaskState = {
  ids: [],
  entities: {},
};

const addTask = (state: TaskState, action): TaskState => {
  const task = action.payload;
  if (state.entities[task._id]) {
    return state;
  }
  const ids = [...state.ids, task._id];
  // use dictionary to add task
  const entities = {...state.entities, [task._id]: task};
  return {...state, ids: ids, entities: entities};
};

const updateTask = (state, action): TaskState => {
  let task: Task;
  if (action.payload.owner._id) {
    task = {
      ...action.payload,
      owner: action.payload.owner._id,
      participants: action.payload.participants.map(p => p._id)
    };
  } else {
    task = {
      ...action.payload,
      owner: action.payload.owner,
      participants: action.payload.participants
    };
  }
  // use dictionary to update task
  const entities = {...state.entities, [task._id]: task};
  return {...state, entities: entities};
};

const loadTasks = (state, action): TaskState => {
  const tasks = action.payload;
  if (tasks === null || tasks.length === 0) {
    return state;
  }
  const incomingIds = tasks.map(p => p._id);
  // get the different from the current tasks and the incoming tasks and only the tasks that the current state
  // does not have will stay and then combine them with the latest state and return back
  const newIds = _.difference(incomingIds, state.ids);
  const incomingEntities = _.chain(tasks)
    .keyBy('_id')
    .mapValues(o => o)
    .value();
  const newEntities = newIds.reduce((entities, id: string) => {
    const newEntity = incomingEntities[id];
    return {
      ...entities,
      [id]: newEntity
    };
  }, {});
  return {
    ...state,
    ids: [...state.ids, ...newIds],
    entities: {...state.entities, ...newEntities}
  };
};

const delTask = (state: TaskState, action): TaskState => {
  const task = action.payload;
  const newIds = state.ids.filter(id => id !== task.id);
  const newEntities = newIds.reduce((entities, id) => {
    // get this entity from the store state and then add it to the new entity object
    const newEntity = state.entities[id];
    // add this entity
    return {...entities, [id]: newEntity};
  }, {});
  return {
    ids: newIds,
    entities: newEntities,
  };
};

// when deleting a project, the tasks belonging to this project should also be removed
const deleteProject = (state, action): TaskState => {
  // the project been deleted
  const project = action.payload;
  // get takslists which the deleted project has
  const taskListIds = project.taskLists;
  console.log(project.taskLists);
  console.log(state.entities);
  const remainingIds: string[] = state.ids.filter(id => taskListIds.indexOf(state.entities[id].taskListId) === -1);
  // get the entities of the remainingIds
    // reduce: the first parameter is the iterating one which been initialized as an empty object
    // the second parameter is the loop of remaining Ids, so basically it should be the task Id
    const remainingEntities = remainingIds.reduce((entities, id) => ({...entities, [id]: state.entities[id]}), {});
  return {
    ids: remainingIds,
    entities: remainingEntities,
  };
};

const moveTask = (state, action) => {
  let task: Task;
  console.log(action.payload);
  if (action.payload.updatedTask.owner._id) {
    task = {
      ...action.payload.updatedTask,
      owner: action.payload.updatedTask.owner._id,
      participants: action.payload.updatedTask.participants.map(p => p._id)
    };
  } else {
    task = {
      ...action.payload.updatedTask,
      owner: action.payload.updatedTask.owner,
      participants: action.payload.updatedTask.participants
    };
  }
  // use dictionary to update task
  const entities = {...state.entities, [task._id]: task};
  return {...state, entities: entities};
};

const moveAllTasks = (state, action): TaskState => {
  const tasks = action.payload.tasks;
  const updatedEntities = tasks.reduce((entities, task) => ({...entities, [task._id]: task}), {});
  return {
    ...state,
    entities: {...state.entities, ...updatedEntities}
  };
};

export function reducer(state: TaskState = initialState, action) {
  switch (action.type) {
    case taskAction.TaskActionTypes.ADD_SUCCESS:
      return addTask(state, action);
    case taskAction.TaskActionTypes.DELETE_SUCCESS:
      return delTask(state, action);
    case taskAction.TaskActionTypes.MOVE_ALL_SUCCESS:
      return moveAllTasks(state, action);
    case taskAction.TaskActionTypes.MOVE_SUCCESS:
      return moveTask(state, action);
    case taskAction.TaskActionTypes.COMPLETE_SUCCESS:
    case taskAction.TaskActionTypes.UPDATE_SUCCESS:
      return updateTask(state, action);
    case taskAction.TaskActionTypes.LOAD_SUCCESS:
      return loadTasks(state, action);
    case projectAction.ProjectActionTypes.DELETE_SUCCESS:
      return deleteProject(state, action);
    default:
      return state;
  }
}

export const getTaskEntities = (state: TaskState) => state.entities;
export const getTaskIds = (state: TaskState) => state.ids;

