import * as fromQuote from './quote.reducer';
import * as fromProject from './project.reducers';
import * as fromTaskList from './task-list.reducer';
import * as fromTask from './task.reducer';
import * as fromUser from './user.reducer';
import * as fromAuth from './auth.reducer';
import {ActionReducer, ActionReducerMap, createFeatureSelector, createSelector, MetaReducer} from '@ngrx/store';
import {localStorageSync} from 'ngrx-store-localstorage';
import {User} from '../../domain';
import * as _ from 'lodash';
import {Task} from "src/app/domain/task.model";
import {getProjectIds} from "./project.reducers";

// root store
export interface State {
  auth: fromAuth.AuthState;
  quote: fromQuote.State;
  project: fromProject.ProjectState;
  taskList: fromTaskList.TaskListState;
  task: fromTask.TaskState;
  user: fromUser.UserState;
}

export const reducers: ActionReducerMap<State> = {
  quote: fromQuote.reducer,
  auth: fromAuth.reducer,
  project: fromProject.reducer,
  taskList: fromTaskList.reducer,
  task: fromTask.reducer,
  user: fromUser.reducer
};

export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  return localStorageSync({keys: ['root'], rehydrate: true})(reducer);
}

export const metaReducers: Array<MetaReducer<any, any>> = [localStorageSyncReducer];

// feature selector is used to get the top state in the index reducer
export const getState = createFeatureSelector<State>('root');

export const getQuoteState = createSelector(
  getState,
  (state: State) => state.quote
);

// for user
export const getUserState = createSelector(
  getState,
  (state: State) => state.user
);

export const getUserEntities = createSelector(
  getUserState,
  fromUser.getUserEntities
);

export const getUsers = createSelector(
  getUserEntities,
  entities => Object.keys(entities).map(id => entities[id])
);

export const getUserById = userId => createSelector(
  getUsers,
  users => users.find(user => user._id === userId)
);

export const getUsersByIds = userIds => createSelector(
  getUsers,
  users => users.filter(user => userIds.indexOf(user._id) > -1)
);

export const getAuthState = createSelector(
  getState,
  (state: State) => state.auth
);

export const getAuthUser = createSelector(
  getAuthState,
  getUsers,
  (state, users) => users.find(u => state.auth.user._id === u._id)
);

// for task lists
export const getTaskListState = createSelector(
  getState,
  (state: State) => state.taskList
);
export const getTaskListEntities = createSelector(
  getTaskListState,
  fromTaskList.getTaskListEntities
);
export const getTaskLists = createSelector(
  getTaskListEntities,
  entities => Object.keys(entities).map(id => entities[id])
);
export const getTaskListIds = createSelector(
  getTaskListState,
  fromTaskList.getTaskListIds
);
export const getMaxListOrder = createSelector(getTaskListEntities, getTaskListIds, (entities, ids) => {
  if (ids.length === 0) {
    return 0;
  }
  const orders: number[] = ids.map(id => entities[id].order);
  return orders.sort()[orders.length - 1];
});

// for task
export const getTaskState = createSelector(
  getState,
  (state: State) => state.task
);

export const getTaskEntities = createSelector(
  getTaskState,
  fromTask.getTaskEntities
);

export const getTasks = createSelector(
  getTaskEntities,
  entities => Object.keys(entities).map(id => entities[id])
);

export const getUserTasks = createSelector(
  getTasks,
  getAuthUser,
  (tasks, user) => {
    return tasks.filter(task => {
      if (task.owner === user._id) {
        return true;
      } else {
        return task.participants.indexOf(user._id) > -1;
      }
    });
  });

export const getTaskById = taskId => createSelector(
  getTasks,
  tasks => tasks.find(task => task._id === taskId)
);

export const getTasksFromTaskListId = taskListId => {
  createSelector(getTasks, tasks => {
    return tasks.filter(task => task.taskListId === taskListId);
  });
};
// for projects
export const getProjectState = createSelector(
  getState,
  (state: State) => state.project
);
export const getProjectEntities = createSelector(
  getProjectState,
  fromProject.getEntities
);
export const getProjects = createSelector(getProjectEntities, entities => {
  return Object.keys(entities).map(id => entities[id]);
});
export const getProjectSelectedId = createSelector(getProjectState, fromProject.getSelectedProjectId);

export const getProjectUsers = (projectId: string) =>
  createSelector(getProjects, getUsers, (projects, users) => {
    const project = projects.find(p => p._id === projectId);
    // user ids in this project type is string array
    const projectMembers = project.members;
    // if the user id is in this project members list, then return the user
    return users.filter(u => projectMembers.indexOf(u._id) > -1);
  });

export const getProjectLists = createSelector(getProjectSelectedId, getTaskLists, (projectId, tasklists) =>
  tasklists.filter(tasklist => tasklist.projectId === projectId));

export const getProjectByTask = (task: Task) => createSelector(
  getProjects,
  getTaskLists,
  (projects, taskLists) => {
    const taskList = taskLists.find(tl => tl._id === task.taskListId);
    projects.find(project => {
      return project.taskLists.indexOf(taskList._id) > -1;
    });
    return projects.find(project => project.taskLists.indexOf(taskList._id) > -1);
  }
);

export const getTaskWithOwners = createSelector(getTasks, getUserEntities, (tasks, userEntities) => {
  return tasks.map(task => {
    return {
      ...task,
      owner: userEntities[task.owner],
      participants: task.participants.map(participant => userEntities[participant])
    };
  });
});

export const getListsWithTasks = createSelector(getProjectLists, getTaskWithOwners, (lists, tasks) => {
  return lists.map(list => {
    return {
      ...list,
      tasks: tasks.filter(task => task.taskListId === list._id)
    };
  });
});


