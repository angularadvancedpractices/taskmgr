import * as userAction from '../actions/user.actions';
import {User} from '../../domain/user.model';

// basically all the logical things should be done in the backend and frontend should be only receiving the results
// for display, but the mock data only serves as a database, so the logic things need to be done in the frontend and
// then show on the page

/*
ids: all users' id been stored in the state
entities: all users are been stored in an object consisting of project with id as the key. Using dictionary makes
userlist edit/update/delete easier
 */
export interface UserState {
  ids: string [];
  entities: { [id: string]: User };
}

export const initialState: UserState = {
  ids: [],
  entities: {}
};

// const addUser = (state: UserState, action): UserState => {
//   const user = action.payload;
//   const newIds = [...state.ids, user.id];
//   // use dictionary to add user
//   const newEntities = {...state.entities, [user.id]: user};
//   return state.entities[user.id] ?
//     {...state, entities: newEntities} :
//     {...state, ids: newIds, entities: newEntities};
// };

const updateUser = (state, action): UserState => {
  let users = [];
  if (action.payload.users) {
    users = action.payload.users;
  } else {
    users = action.payload;
  }
  const userObjects = users.reduce((entities, obj) => ({...entities, [obj.user._id]: obj.user}), {});
  const newEntities = {...state.entities, ...userObjects};
  return {...state, users: newEntities};
};

const loadUsers = (state, action): UserState => {
  const users = <User[]>action.payload;
  if (users === null) {
    return state;
  }
  const newUsers = users.filter(user => !state.entities[user._id]);
  const newIds = newUsers.map(user => user._id);
  if (newIds.length === 0) {
    return state;
  }
  const newEntities = newUsers.reduce((entities, obj) => ({...entities, [obj._id]: obj}), {});
  return {
    ids: [...state.ids, ...newIds],
    entities: {...state.entities, ...newEntities}
  };
};

// const delUser = (state: UserState, action): UserState => {
//   const user = action.payload;
//   const newEntities = {...state.entities, [user.id]: user};
//   return state.entities[user.id]
//     ? {...state, entities: newEntities}
//     : state;
// };

export function reducer(state: UserState = initialState, action) {
  switch (action.type) {
    case userAction.UserActionTypes.ADD_SUCCESS:
    // return addUser(state, action);
    case userAction.UserActionTypes.DELETE_SUCCESS:
    // return delUser(state, action);
    case userAction.UserActionTypes.UPDATE_SUCCESS:
      return updateUser(state, action);
    case userAction.UserActionTypes.SEARCH_SUCCESS:
    case userAction.UserActionTypes.LOAD_SUCCESS:
      return loadUsers(state, action);
    default:
      return state;
  }
}

export const getUserEntities = (state: UserState) => state.entities;

