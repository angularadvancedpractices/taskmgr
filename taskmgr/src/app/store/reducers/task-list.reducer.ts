import * as taskListAction from '../actions/task-list.actions';
import * as projectAction from '../actions/project.actions';
import * as taskAction from '../actions/task.actions';
import * as _ from 'lodash';
import {TaskList} from '../../domain/task-list.model';

// basically all the logical things should be done in the backend and frontend should be only receiving the results
// for display, but the mock data only serves as a database, so the logic things need to be done in the frontend and
// then show on the page

/*
ids: all tasks' id been stored in the state
entities: all tasks are been stored in an object consisting of project with id as the key. Using dictionary makes
tasklist edit/update/delete easier
 */
export interface TaskListState {
  ids: string[];
  entities: { [id: string]: TaskList };
  selectedIds: string[] | null;
}

export const initialState: TaskListState = {
  ids: [],
  entities: {},
  selectedIds: []
};

const addTaskList = (state: TaskListState, action): TaskListState => {
  const taskList = action.payload;
  if (state.entities[taskList._id]) {
    return state;
  }
  // console.log(state);
  // console.log(taskList);
  const ids = [...state.ids, taskList._id];
  // use dictionary to add taskList
  const entities = {...state.entities, [taskList._id]: taskList};
  // console.log({...state, ids: ids, entities: entities});
  return {
    ids: [...state.ids, ...ids],
    entities: {...state.entities, ...entities},
    selectedIds: null
  };
};

const updateTaskList = (state, action): TaskListState => {
  const taskList = action.payload;
  // use dictionary to update taskList
  const entities = {...state.entities, [taskList._id]: taskList};
  return {...state, entities: entities};
};

const loadTaskLists = (state, action): TaskListState => {
  const taskLists = <TaskList[]>action.payload;
  // if taskList is null then return the orginal state
  if (taskLists === null) {
    return state;
  }
  const newTaskLists = taskLists.filter(taskList => !state.entities[taskList._id]);
  if (newTaskLists.length === 0) {
    return state;
  }
  const newIds = newTaskLists.map(taskList => taskList._id);
  const newEntities = newTaskLists.reduce((entities, obj) => ({...entities, [obj._id]: obj}), {});
  return {
    ids: [...state.ids, ...newIds],
    entities: {...state.entities, ...newEntities},
    selectedIds: [...newIds]
  };
};

const delTaskList = (state: TaskListState, action): TaskListState => {
  const taskList = action.payload;
  const newIds = state.ids.filter(id => id !== taskList._id);
  const newEntities = newIds.reduce((entities, id) => {
    // get this entity from the store state and then add it to the new entity object
    const newEntity = state.entities[id];
    // add this entity
    return {...entities, [id]: newEntity};
  }, {});
  // const newSelectedIds = state.selectedIds.filter(id => id !== taskList.id);
  return {
    ...state,
    ids: newIds,
    entities: newEntities,
  };
};

const swapTaskLists = (state, action) => {
  const taskLists = action.payload;
  const updatedEntities = _.chain(taskLists)
    .keyBy('_id')
    .mapValues(o => o)
    .value();
  const newEntities = {...state.entities, ...updatedEntities};
  return {
    ...state,
    entities: newEntities
  };
};

const moveTaskSuccess = (state, action) => {
  const taskListRemovedTaskId = action.payload.taskListRemovedTaskId;
  const taskListAddedTaskId = action.payload.taskListAddedTaskId;
  // use dictionary to update taskList
  const entities = {
    ...state.entities,
    [taskListRemovedTaskId._id]: taskListRemovedTaskId,
    [taskListAddedTaskId._id]: taskListAddedTaskId
  };
  return {...state, entities: entities};
};

const moveAllTasksSuccess = (state, action) => {
  const srcListId = action.payload.srcListId;
  const targetListId = action.payload.targetListId;
  const srcTasks = action.payload.tasks;
  return {
    ...state.entities,
    [srcListId]: state.entities[srcListId],
    [targetListId]: state.entities[targetListId]
  };
};

const deleteProject = (state, action) => {
  // the project been deleted
  const project = action.payload;
  console.log(state.entities);
  // get takslists which the deleted project has
  const remainingIds: string[] = state.ids.filter(id => state.entities[id].projectId !== project._id);
  // get the entities of the remainingIds
  // reduce: the first parameter is the iterating one which been initialized as an empty object
  // the second parameter is the loop of remaining Ids, so basically it should be the task Id
  const remainingEntities = remainingIds.reduce((entities, id) => ({...entities, [id]: state.entities[id]}), {});
  return {
    ids: remainingIds,
    entities: remainingEntities,
  };
};

export function reducer(state: TaskListState = initialState, action) {
  switch (action.type) {
    case taskListAction.TaskListActionTypes.ADD_SUCCESS:
      return addTaskList(state, action);
    case taskListAction.TaskListActionTypes.DELETE_SUCCESS:
      return delTaskList(state, action);
    case taskListAction.TaskListActionTypes.SWAP_SUCCESS:
      return swapTaskLists(state, action);
    case taskListAction.TaskListActionTypes.UPDATE_SUCCESS:
      return updateTaskList(state, action);
    case taskListAction.TaskListActionTypes.LOAD_SUCCESS:
      return loadTaskLists(state, action);
    case taskAction.TaskActionTypes.MOVE_SUCCESS:
      return moveTaskSuccess(state, action);
    case taskAction.TaskActionTypes.MOVE_ALL_SUCCESS:
      return moveAllTasksSuccess(state, action);
    case projectAction.ProjectActionTypes.DELETE_SUCCESS:
      return deleteProject(state, action);
    default:
      return state;
  }
}

export const getTaskListEntities = (state: TaskListState) => state.entities;
export const getTaskListSelectedIds = (state: TaskListState) => state.selectedIds;
export const getTaskListIds = (state: TaskListState) => state.ids;

