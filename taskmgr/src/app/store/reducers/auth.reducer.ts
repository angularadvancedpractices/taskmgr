import {Auth} from '../../domain/auth.model';
import {AuthActionTypes, AuthActions} from '../actions/auth.actions';

export interface AuthState {
  auth: Auth;
  isLoggedIn: boolean;
  loginFail: boolean;
  registerFail: boolean;
}

export const initialState: AuthState = {
  auth: {},
  isLoggedIn: false,
  loginFail: false,
  registerFail: false,
};

export function reducer(state = initialState, action: AuthActions): AuthState {

  switch (action.type) {
    // case AuthActionTypes.REGISTER_SUCCESS: {
    //   const auth = action.payload;
    //   return {
    //     ...initialState,
    //     isLoggedIn: true,
    //     registerFail: false,
    //     auth: {
    //       token: auth.token,
    //     }
    //   };
    // }
    case AuthActionTypes.LOGIN_SUCCESS: {
      const auth = action.payload;
      return {
        ...initialState,
        loginFail: false,
        isLoggedIn: true,
        auth: {
          token: auth.token,
          user: auth.user
        }
      };
    }

    case AuthActionTypes.LOGIN_FAILURE: {
      return {
        ...initialState,
        loginFail: true
      };
    }
    case AuthActionTypes.REGISTER_FAILURE: {
      return {
        ...initialState,
        registerFail: true
      };
    }

    case AuthActionTypes.LOGOUT: {
      if (localStorage.getItem('root') && localStorage.getItem('currentUserToken')) {
        localStorage.removeItem('root');
        localStorage.removeItem('currentUserToken');
      }
      return initialState;
    }

    default:
      return state;
  }

}
