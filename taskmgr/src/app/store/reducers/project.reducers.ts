import * as projectActions from '../actions/project.actions';
import {Project} from '../../domain/project.model';
import * as authActions from '../actions/auth.actions';
import * as _ from 'lodash';
/*
ids: all projects' id stored in the state
entities: an object which stores all projects. every project is stored with its project id as key
selectedId: the id been selected by user (maybe through a click)
 */
// use dictionary to make add/update/edit/delete project easier
export interface ProjectState {
  ids: string[];
  entities: { [id: string]: Project };
  selectedId: string | null;
}

export const initialState: ProjectState = {
  ids: [],
  entities: {},
  selectedId: null
};

export function reducer(state = initialState, action: projectActions.ProjectActions): ProjectState {
  switch (action.type) {
    case projectActions.ProjectActionTypes.ADD_SUCCESS:
      return addProject(state, action);
    case projectActions.ProjectActionTypes.DELETE_SUCCESS:
      return delProject(state, action);
    case projectActions.ProjectActionTypes.INVITE_SUCCESS:
    case projectActions.ProjectActionTypes.UPDATE_SUCCESS:
      return updateProject(state, action);
    case projectActions.ProjectActionTypes.LOAD_SUCCESS:
      return loadProjects(state, action);
    case projectActions.ProjectActionTypes.SELECT_PROJECT:
      return {...state, selectedId: action.payload._id};
    default:
      return state;
  }
}

const addProject = (state: ProjectState, action): ProjectState => {
  const project = action.payload;
  // console.log(project);
  if (state.entities[project._id]) {
    return state;
  }
  const ids = [...state.ids, project._id];
  // use dictionary to add project
  const entities = {...state.entities, [project._id]: project};
  return {...state, ids: ids, entities: entities};
};

const updateProject = (state, action): ProjectState => {
  const project = action.payload;
  const entities = {...state.entities, [project._id]: project};
  return {...state, entities: entities};
};

const loadProjects = (state, action): ProjectState => {
  const projects = action.payload;
  // console.log(projects);
  if (projects === null) {
    return state;
  }
  const newProjects = projects.filter(project => !state.entities[project._id]);
  const newIds = newProjects.map(project => project._id);
  if (newProjects.length === 0) {
    return state;
  }
  const newEntities = newProjects.reduce((entities, obj) => ({...entities, [obj._id]: obj}), {});
  // console.log({...state.entities, ...newEntities});
  return {
    ids: [...state.ids, ...newIds],
    entities: {...state.entities, ...newEntities},
    selectedId: null
  };
};

const delProject = (state: ProjectState, action): ProjectState => {
  const project = action.payload;
  const newIds = state.ids.filter(id => id !== project._id);
  const newEntities = newIds.reduce((entities, id) => {
    // get this entity from the store state and then add it to the new entity object
    const newEntity = state.entities[id];
    // add this entity
    return {...entities, [id]: newEntity};
    }, {});
  return {
    ids: newIds,
    entities: newEntities,
    selectedId: null
  };
};


export const getEntities = (state: ProjectState) => state.entities;
export const getProjectIds = (state: ProjectState) => state.ids;
export const getSelectedProjectId = (state: ProjectState) => state.selectedId;
