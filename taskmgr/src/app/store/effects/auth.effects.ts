import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Observable, of} from 'rxjs/index';
import * as auth from '../actions/auth.actions';
import {Action} from '@ngrx/store';
import {AuthService} from '../../services/auth.service';
import {catchError, concatMap, map, switchMap, tap} from 'rxjs/internal/operators';
import {Router} from '@angular/router';
import {MatSnackBar} from "@angular/material";

@Injectable()
export class AuthEffects {

  @Effect()
  login$: Observable<Action> = this.action$.pipe(
    ofType<auth.Login>(auth.AuthActionTypes.LOGIN),
    map(action => action.payload),
    switchMap(inputUser =>
      this.authService$.login(inputUser.email, inputUser.password)
        .pipe(
          map(authData => new auth.LoginSuccess(authData)),
          catchError(err => {
            console.log(err);
            return of(new auth.LoginFailure(err));
          }))
    )
  );

  @Effect()
  register$: Observable<Action> = this.action$.pipe(
    ofType<auth.Register>(auth.AuthActionTypes.REGISTER),
    map(action => action.payload),
    switchMap(inputUser => {
        return this.authService$.register(inputUser)
          .pipe(
            map(r => new auth.RegisterSuccess(r)),
            catchError(err => of(new auth.RegisterFailure(err)))
          );
      }
    )
  );

  @Effect()
  forget$: Observable<Action> = this.action$.pipe(
    ofType<auth.ForgetPassword>(auth.AuthActionTypes.FORGET),
    map(action => action.payload),
    switchMap(email => {
        return this.authService$.forgetPassword(email)
          .pipe(
            map(r => new auth.ForgetPasswordSuccess(null)),
            catchError(err => {
              if (err.status === 404) {
                this.matSnackBar.open('cannot find such user, please try again', 'dismiss', {
                  duration: 3000
                })
              }
              return of(new auth.ForgetPasswordFail(err));
            })
          );
      }
    )
  );

  @Effect()
  reset$: Observable<Action> = this.action$.pipe(
    ofType<auth.ResetPassword>(auth.AuthActionTypes.RESET),
    map(action => action.payload),
    switchMap(resetModel => {
        return this.authService$.resetPassword(resetModel)
          .pipe(
            map(r => new auth.ResetPasswordSuccess(null)),
            catchError(err => of(new auth.ResetPasswordFail(err)))
          );
      }
    )
  );

  @Effect({dispatch: false})
  logout$: Observable<Action> = this.action$.pipe(
    ofType(auth.AuthActionTypes.LOGOUT),
    tap(_ => {
      this.authService$.logout();
      this.router.navigate(['/']);
      }
    ));
  // if the effect does not dispatch an action, then use dispatch: false to end it otherwise it will go to an
  // endless loop
  @Effect({dispatch: false})
  loginAndRedirect$: Observable<Action> = this.action$.pipe(
    ofType(auth.AuthActionTypes.LOGIN_SUCCESS),
    tap(_ => this.router.navigate(['/project']))
  );

  @Effect({dispatch: false})
  registerAndRedirect$: Observable<Action> = this.action$.pipe(
    ofType(auth.AuthActionTypes.REGISTER_SUCCESS),
    tap(_ => this.router.navigate(['/login'], {queryParams: {from: 'register'}}))
  );

  @Effect({dispatch: false})
  resetAndRedirect$: Observable<Action> = this.action$.pipe(
    ofType(auth.AuthActionTypes.RESET_SUCCESS),
    tap(_ => this.router.navigate(['/login'], {queryParams: {from: 'reset'}}))
  );

  @Effect({dispatch: false})
  forgetAndRedirect$: Observable<Action> = this.action$.pipe(
    ofType(auth.AuthActionTypes.FORGET_SUCCESS),
    tap(_ => this.router.navigate(['/login'], {queryParams: {from: 'forget'}}))
  );

  constructor(private action$: Actions,
              private authService$: AuthService,
              private router: Router,
              private matSnackBar: MatSnackBar) {
  }
}
