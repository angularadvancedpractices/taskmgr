import {Actions, Effect, ofType} from '@ngrx/effects';
import {Observable, of} from 'rxjs/index';
import {Action} from '@ngrx/store';
import * as userActions from '../actions/user.actions';
import {catchError, concatMap, map, mergeMap, switchMap, tap} from 'rxjs/internal/operators';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {Injectable} from '@angular/core';

@Injectable()
export class UserEffects {

  /*
  是一个隐形的需要被其他地方进行触发
   */
  @Effect()
  loadUsers$: Observable<Action> = this.actions$
    .pipe(
      ofType<userActions.LoadUser>(userActions.UserActionTypes.LOAD),
      map(action => action.payload),
      // here to use concat map to get all data from back
      concatMap((projectId) => {
        return this.userService$.getUsersByProjectId(projectId)
          .pipe(
            map(users => {
              return new userActions.LoadUserSuccess(users);
            }),
            catchError(err => of(new userActions.LoadUserFailure(err)))
          );
      })
    );

  @Effect()
  addUserProjectRef$: Observable<Action> = this.actions$
    .pipe(
      ofType<userActions.AddUser>(userActions.UserActionTypes.ADD),
      map(action => action.payload),
      switchMap(({user, projectId}) => {
        return this.userService$.addProjectRef(user, projectId)
          .pipe(
            map(p => new userActions.AddUserSuccess(p)),
            catchError(err => of(new userActions.AddUserFailure(err)))
          );
      }));

  @Effect()
  updateUserProjectRef$: Observable<Action> = this.actions$
    .pipe(
      ofType<userActions.UpdateUser>(userActions.UserActionTypes.UPDATE),
      map(action => action.payload),
      switchMap((project) => this.userService$.batchUpdateProjectRef(project)
        .pipe(
          map(users => new userActions.UpdateUserSuccess(users)),
          catchError(err => of(new userActions.UpdateUserFailure(err)))
        ))
    );

  @Effect()
  delUserProjectRef$: Observable<Action> = this.actions$
    .pipe(
      ofType<userActions.DeleteUser>(userActions.UserActionTypes.DELETE),
      map(action => action.payload),
      switchMap(({user, projectId}) => this.userService$.removeProjectRef(user, projectId)
        .pipe(
          map(u => new userActions.DeleteUserSuccess(u)),
          catchError(err => of(new userActions.UpdateUserFailure(err)))
        ))
    );

  @Effect()
  search$: Observable<Action> = this.actions$
    .pipe(
      ofType<userActions.SearchUser>(userActions.UserActionTypes.SEARCH),
      map(action => action.payload),
      switchMap(search => this.userService$.searchUsers(search)
        .pipe(
          map(users => new userActions.SearchUserSuccess(users)),
          catchError(err => of(new userActions.SearchUserFailure(err)))))
    );

  constructor(private actions$: Actions,
              private userService$: UserService,
              private router: Router) {
  }
}
