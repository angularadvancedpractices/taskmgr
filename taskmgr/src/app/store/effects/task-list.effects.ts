import {Actions, Effect, ofType} from '@ngrx/effects';
import {from, Observable, of} from 'rxjs/index';
import {Action, Store} from '@ngrx/store';
import * as taskListActions from '../actions/task-list.actions';
import * as taskActions from '../actions/task.actions';
import {catchError, map, mergeMap, switchMap, tap} from 'rxjs/internal/operators';
import {TaskListService} from '../../services/task-list.service';
import {Router} from '@angular/router';
import {Injectable} from '@angular/core';
import * as fromRoot from '../../store/reducers';

@Injectable()
export class TaskListEffects {

  @Effect()
  loadTaskLists$: Observable<Action> = this.actions$
    .pipe(
      ofType<taskListActions.LoadTaskLists>(taskListActions.TaskListActionTypes.LOAD),
      map(action => action.payload),
      switchMap((projectId) => {
        return this.taskListService$.get(projectId)
          .pipe(
            map(taskList => new taskListActions.LoadTaskListSuccess(taskList)),
            catchError(err => of(new taskListActions.LoadTaskListFailure(err)))
          );
      })
    );

  @Effect()
  loadAllTaskLists$: Observable<Action> = this.actions$
    .pipe(
      ofType<taskListActions.LoadTaskLists>(taskListActions.TaskListActionTypes.LOAD_ALL),
      map(action => action.payload),
      switchMap(_ => {
        return this.taskListService$.getAll()
          .pipe(
            map(taskList => new taskListActions.LoadTaskListSuccess(taskList)),
            catchError(err => of(new taskListActions.LoadTaskListFailure(err)))
          );
      })
    );

  @Effect()
  loadTasks$: Observable<Action> = this.actions$
    .pipe(
      ofType<taskListActions.LoadTaskListSuccess>(taskListActions.TaskListActionTypes.LOAD_SUCCESS),
      map(action => action.payload),
      mergeMap(taskLists => {
        return from(taskLists)
          .pipe(
            map(taskList => taskList._id),
            map(taskListId => new taskActions.LoadTask(taskListId)),
            catchError(err => of(new taskActions.LoadTaskFailure(err)))
          );
      })
    );

  @Effect()
  addTaskList$: Observable<Action> = this.actions$
    .pipe(
      ofType<taskListActions.AddTaskList>(taskListActions.TaskListActionTypes.ADD),
      map(action => action.payload),
      switchMap((result) => {
        return this.taskListService$.add(result)
          .pipe(
            map(p => new taskListActions.AddTaskListSuccess(p)),
            catchError(err => of(new taskListActions.AddTaskListFailure(err)))
          );
      }));

  @Effect()
  updateTaskList$: Observable<Action> = this.actions$
    .pipe(
      ofType<taskListActions.UpdateTaskList>(taskListActions.TaskListActionTypes.UPDATE),
      map(action => action.payload),
      switchMap((taskList) => this.taskListService$.update(taskList)
        .pipe(
          map(updatedTaskList => new taskListActions.UpdateTaskListSuccess(updatedTaskList)),
          catchError(err => of(new taskListActions.UpdateTaskListFailure(err)))
        ))
    );

  @Effect()
  delTaskList$: Observable<Action> = this.actions$
    .pipe(
      ofType<taskListActions.DeleteTaskList>(taskListActions.TaskListActionTypes.DELETE),
      map(action => action.payload),
      switchMap(payload => this.taskListService$.del(payload.taskList, payload.projectId)
        .pipe(
          map(_ => new taskListActions.DeleteTaskListSuccess(payload.taskList)),
          catchError(err => of(new taskListActions.UpdateTaskListFailure(err)))
        ))
    );

  @Effect()
  swap$: Observable<Action> = this.actions$
    .pipe(
      ofType<taskListActions.SwapTaskList>(taskListActions.TaskListActionTypes.SWAP),
      map(action => action.payload),
      switchMap(taskLists => this.taskListService$.swapOrder(taskLists[0], taskLists[1])
        .pipe(
          map(resultTaskLists => new taskListActions.SwapTaskListSuccess(resultTaskLists)),
          catchError(err => of(new taskListActions.SwapTaskListFailure(err)))
        ))
    );

  constructor(private actions$: Actions,
              private taskListService$: TaskListService,
              private router: Router,
              private store$: Store<fromRoot.State>) {
  }
}
