import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {defer, Observable, of} from 'rxjs/index';
import * as quote from '../actions/quote.action';
import {Action} from '@ngrx/store';
import {QuoteService} from '../../services/quote.service';
import {catchError, map, switchMap} from 'rxjs/internal/operators';

@Injectable()
export class QuoteEffects {

  @Effect()
  quote$: Observable<Action> = this.action$
    .pipe(
      ofType<quote.GetQuote>(quote.QuoteActionTypes.QUOTE),
      switchMap(_ => this.quoteService$.getQuote()),
      map(q => new quote.GetQuoteSuccess(q)),
      catchError(err => of(new quote.GetQuoteFailure(err)))
    );

  constructor(private action$: Actions,
              private quoteService$: QuoteService) {
  }
}
