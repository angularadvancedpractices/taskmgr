import {Actions, Effect, ofType} from '@ngrx/effects';
import {Observable, of} from 'rxjs/index';
import {Action, Store} from '@ngrx/store';
import * as taskActions from '../actions/task.actions';
import * as fromRoot from '../reducers';
import {catchError, map, mergeMap, switchMap, tap, withLatestFrom} from 'rxjs/internal/operators';
import {TaskService} from '../../services/task.service';
import {Router} from '@angular/router';
import {Injectable} from '@angular/core';
import {Task} from '../../domain';

@Injectable()
export class TaskEffects {

  @Effect()
  loadTasks$: Observable<Action> = this.actions$
    .pipe(
      ofType<taskActions.LoadTask>(taskActions.TaskActionTypes.LOAD),
      map(action => action.payload),
      mergeMap(taskListId => {
        return this.taskService$.get(taskListId)
          .pipe(
            map(tasks => new taskActions.LoadTaskSuccess(tasks)),
            catchError(err => of(new taskActions.LoadTaskFailure(err)))
          );
      })
    );

  @Effect()
  loadAllTasks$: Observable<Action> = this.actions$
    .pipe(
      ofType<taskActions.LoadTask>(taskActions.TaskActionTypes.LOAD_ALL_TASKS),
      map(n => n),
      mergeMap(_ => {
        return this.taskService$.getAll()
          .pipe(
            map(tasks => {
              console.log(tasks);
              return new taskActions.LoadTaskSuccess(tasks)
            }),
            catchError(err => of(new taskActions.LoadTaskFailure(err)))
          );
      })
    );

  @Effect()
  addTask$: Observable<Action> = this.actions$
    .pipe(
      ofType<taskActions.AddTask>(taskActions.TaskActionTypes.ADD),
      map(action => action.payload),
      switchMap(payload => {
        return this.taskService$.add(payload.task, payload.taskListId)
          .pipe(
            map(task => new taskActions.AddTaskSuccess(task)),
            catchError(err => of(new taskActions.AddTaskFailure(err)))
          );
      }));

  @Effect()
  updateTask$: Observable<Action> = this.actions$
    .pipe(
      ofType<taskActions.UpdateTask>(taskActions.TaskActionTypes.UPDATE),
      map(action => action.payload),
      switchMap(task => {
        return this.taskService$.update(task)
          .pipe(
            map(t => new taskActions.UpdateTaskSuccess(t)),
            catchError(err => of(new taskActions.UpdateTaskFailure(err)))
          );
      })
    );

  @Effect()
  delTask$: Observable<Action> = this.actions$
    .pipe(
      ofType<taskActions.DeleteTask>(taskActions.TaskActionTypes.DELETE),
      map(action => action.payload),
      switchMap(task => this.taskService$.del(task)
        .pipe(
          map(_ => new taskActions.DeleteTaskSuccess(task)),
          catchError(err => of(new taskActions.UpdateTaskFailure(err)))
        ))
    );

  @Effect()
  complete$: Observable<Action> = this.actions$
    .pipe(
      ofType<taskActions.CompleteTask>(taskActions.TaskActionTypes.COMPLETE),
      map(action => action.payload),
      switchMap(task => this.taskService$.complete(task)
        .pipe(
          map(updatedTask => new taskActions.CompleteTaskSuccess(updatedTask)),
          catchError(err => of(new taskActions.CompleteTaskFailure(err)))
        ))
    );

  @Effect()
  move$: Observable<Action> = this.actions$
    .pipe(
      ofType<taskActions.MoveTask>(taskActions.TaskActionTypes.MOVE),
      map(action => action.payload),
      switchMap(({task, targetListId}) => this.taskService$.move(task, targetListId)
        .pipe(
          map(tasks => new taskActions.MoveTaskSuccess(tasks)),
          catchError(err => of(new taskActions.MoveTaskFailure(err)))))
    );

  @Effect()
  moveAll$: Observable<Action> = this.actions$
    .pipe(
      ofType<taskActions.MoveAll>(taskActions.TaskActionTypes.MOVE_ALL),
      map(action => action.payload),
      switchMap(({srcListId, targetListId}) => this.taskService$.moveAll(srcListId, targetListId)
        .pipe(
          map(res => {
            console.log(res);
            return new taskActions.MoveAllSuccess(res);
          }),
          catchError(err => of(new taskActions.MoveAllFailure(err)))))
    );

  constructor(private actions$: Actions,
              private taskService$: TaskService,
              private router: Router,
              private store$: Store<fromRoot.State>) {
  }
}
