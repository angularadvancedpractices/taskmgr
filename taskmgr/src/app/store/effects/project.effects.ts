import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {from, Observable, of} from 'rxjs/index';
import * as projectActions from '../actions/project.actions';
import * as taskListActions from '../actions/task-list.actions';
import * as userActions from '../actions/user.actions';
import * as authActions from '../actions/auth.actions';
import {catchError, map, switchMap, tap, withLatestFrom} from 'rxjs/internal/operators';
import {ProjectService} from '../../services/project.service';
import {Action, Store} from '@ngrx/store';
import {AddProjectFailure, AddProjectSuccess} from '../actions/project.actions';
import {Router} from '@angular/router';
import * as fromRoot from '../reducers/index';
import {State} from '../reducers/index';
import {Project} from '../../domain/project.model';

@Injectable()
export class ProjectEffects {
  @Effect()
  loadProjects$: Observable<Action> = this.actions$
    .pipe(
      ofType(projectActions.ProjectActionTypes.LOAD),
      // use withLatestFrom to combine the latest auth data to load which projects
      // NgRX is based on RXJS, so all RXJS operators can be used by NgRX
      withLatestFrom(this.store$.select(fromRoot.getAuthState)),
      switchMap(([_, auth]) => {
        return this.projectService$.getProjectsByUserId(auth.auth.user._id)
          .pipe(
            map(res => {
              return new projectActions.LoadProjectSuccess(res);
            }),
            catchError(err => of(new authActions.Logout(err)))
          );
      })
    );

  @Effect()
  loadUsersAfterProjectLoaded$: Observable<Action> = this.actions$
    .pipe(
      ofType<projectActions.LoadProjectSuccess>(projectActions.ProjectActionTypes.LOAD_SUCCESS),
      map(action => action.payload),
      switchMap((projects: Project[]) => {
        return from(projects.map(project => project._id))
          .pipe(
            map(projectId => {
              return new userActions.LoadUser(projectId);
            })
          );
      })
    );

  @Effect()
  invite$: Observable<Action> = this.actions$
    .pipe(
      ofType<projectActions.InviteProject>(projectActions.ProjectActionTypes.INVITE),
      map(action => action.payload),
      switchMap(({projectId, members}) => {
          return this.projectService$.invite(projectId, members)
            .pipe(
              map((project) => {
                return new projectActions.InviteProjectSuccess(project);
              }),
              catchError(err => of(new projectActions.InviteProjectFailure(err)))
            );
        }
      )
    );

  @Effect()
  loadUsersAfterProjectMembersUpdated$: Observable<Action> = this.actions$
    .pipe(
      ofType<projectActions.InviteProjectSuccess>(projectActions.ProjectActionTypes.INVITE_SUCCESS),
      map(action => action.payload),
      map((project) => {
          return new userActions.LoadUser(project._id);
        }
      )
    );

  @Effect()
  addProject$: Observable<Action> = this.actions$
    .pipe(
      ofType<projectActions.AddProject>(projectActions.ProjectActionTypes.ADD),
      map(action => action.payload),
      withLatestFrom(this.store$.select(fromRoot.getAuthState)),
      switchMap(([project, auth]) => {
        // add current user id since it will be a member of this project
        const toBeAdd = {...project, members: [`${auth.auth.user._id}`]};
        return this.projectService$.add(toBeAdd)
          .pipe(
            map(p => new AddProjectSuccess(p)),
            catchError(err => of(new AddProjectFailure(err)))
          );
      }));

  @Effect()
  updateProject$: Observable<Action> = this.actions$
    .pipe(
      ofType<projectActions.UpdateProject>(projectActions.ProjectActionTypes.UPDATE),
      map(action => action.payload),
      // withLatestFrom(this.store$.select(fromRoot.getAuthState)),
      switchMap((project) => this.projectService$.update(project)
        .pipe(
          map(_ => new projectActions.UpdateProjectSuccess(project)),
          catchError(err => of(new projectActions.UpdateProjectFailure(err)))
        ))
    );

  @Effect()
  delProject$: Observable<Action> = this.actions$
    .pipe(
      ofType<projectActions.DeleteProject>(projectActions.ProjectActionTypes.DELETE),
      map(action => action.payload),
      switchMap(project => this.projectService$.del(project)
        .pipe(
          map(_ => new projectActions.DeleteProjectSuccess(project)),
          catchError(err => of(new projectActions.UpdateProjectFailure(err)))
        ))
    );

  @Effect({dispatch: false})
  selectProject$: Observable<Project> = this.actions$
    .pipe(
      ofType<projectActions.SelectProject>(projectActions.ProjectActionTypes.SELECT_PROJECT),
      map(action => action.payload),
      tap(project => this.router.navigate([`/tasks/${project._id}`]))
    );

  @Effect()
  loadTaskLists$: Observable<Action> = this.actions$
    .pipe(
      ofType<projectActions.SelectProject>(projectActions.ProjectActionTypes.SELECT_PROJECT),
      map(action => action.payload),
      map(project => new taskListActions.LoadTaskLists(project._id))
    );

  constructor(private actions$: Actions,
              private projectService$: ProjectService,
              private router: Router,
              private store$: Store<State>) {
  }
}
