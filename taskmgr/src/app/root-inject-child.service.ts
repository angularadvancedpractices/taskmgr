import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RootInjectChildService {

  darkTheme: any;

  constructor() { }
}
