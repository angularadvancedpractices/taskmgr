import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import * as fromRoot from '../../store/reducers';
import {Task} from '../../domain';
import {Store} from "@ngrx/store";
import {getUserById, getUsersByIds} from "../../store/reducers/index";
import {User} from "../../domain/user.model";

@Component({
  selector: 'app-calendar-event-dialog',
  templateUrl: './calendar-event-dialog.component.html',
  styleUrls: ['./calendar-event-dialog.component.scss']
})
export class CalendarEventDialogComponent implements OnInit {

  task: Task;
  taskOwner: User;
  taskParticipants: User[];

  ngOnInit(): void {
    this.store$.select(getUserById(this.data.task.owner)).subscribe(owner => this.taskOwner = owner);
    this.store$.select(getUsersByIds(this.data.task.participants)).subscribe(participants => {
      this.taskParticipants = participants
    });
    this.task = this.data.task;
  }

  constructor(
    public dialogRef: MatDialogRef<CalendarEventDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private store$: Store<fromRoot.State>) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
