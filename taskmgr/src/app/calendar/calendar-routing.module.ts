import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CalendarHomeComponent} from './calendar-home/calendar-home.component';
import {AuthGuardService} from "../services/auth-guard.service";

const routes: Routes = [
  {
    path: 'calendar', component: CalendarHomeComponent, canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CalendarRoutingModule {
}
