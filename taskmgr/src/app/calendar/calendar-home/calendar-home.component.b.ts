import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import * as fromRoot from '../../store/reducers';
import {Store} from "@ngrx/store";
import {CalendarEvent} from 'angular-calendar'
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';
import {colors} from '../demo-utils/colors';
import {Observable, Subject} from "rxjs/index";
import {Task} from '../../domain';
import {EventColor} from 'calendar-utils';
import {Subscription} from 'rxjs/internal/Subscription';
import {map} from "rxjs/internal/operators";

@Component({
  selector: 'app-calendar-home',
  template: `
    <mwl-demo-utils-calendar-header [(view)]="view" [(viewDate)]="viewDate">
    </mwl-demo-utils-calendar-header>
    <ng-template #loading>
      <div class="text-center">
        <i class="fa fa-spin fa-spinner fa-5x"></i> <br/>
        Loading events...
      </div>
    </ng-template>
    <div *ngIf="events$ | async; else loading; let events">
      <div [ngSwitch]="view" style="font-family: 'Microsoft YaHei Light'; margin: 0 20px">
        <mwl-calendar-month-view
          *ngSwitchCase="'month'"
          (dayClicked)="this.dayClicked($event.day)"
          (eventClicked)="handleEvent('Clicked', $event.event)"
          [viewDate]="viewDate"
          [activeDayIsOpen]="activeDayIsOpen"
          [events]="events"
        >
        </mwl-calendar-month-view>
        <mwl-calendar-week-view
          *ngSwitchCase="'week'"
          (eventClicked)="handleEvent('Clicked', $event.event)"
          [viewDate]="viewDate"
          [events]="events"
        >
        </mwl-calendar-week-view>
        <mwl-calendar-day-view
          *ngSwitchCase="'day'"
          (eventClicked)="handleEvent('Clicked', $event.event)"
          [viewDate]="viewDate"
          [events]="events"
        >
        </mwl-calendar-day-view>
      </div>
    </div>
  `,
  styles: []
})
export class CalendarHomeComponent implements OnInit {
  view: string = 'month';
  viewDate: Date = new Date();
  events$: Observable<Array<CalendarEvent<{ task: Task }>>>;
  tasks$: Observable<Task[]>;
  activeDayIsOpen: boolean = true;
  // refresh: Subject<any> = new Subject();
  // subscription: Subscription;

  constructor(private store$: Store<fromRoot.State>,
              private cd: ChangeDetectorRef) {
  }

  dayClicked({date, events}: { date: Date; events: Array<CalendarEvent<{ task: Task }>> }): void {
    // console.log(date);
    // console.log(events);
    if (isSameMonth(date, this.viewDate)) {
      if ((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) || events.length === 0) {
        this.activeDayIsOpen = false;
      } else {
        this.viewDate = date;
        this.activeDayIsOpen = true;
        console.log(this.activeDayIsOpen);
        console.log(this.viewDate);
      }
    }
  }

  getPriorityColor(priority: number): EventColor {
    if (priority === 1) {
      return colors.red;
    } else return priority === 2 ? colors.orange : colors.grey;
  }

  ngOnInit() {
    this.tasks$ = this.store$.select(fromRoot.getUserTasks);
    // this.subscription = this.tasks$.subscribe(tasks => tasks.forEach(task => {
    //   const event: CalendarEvent = {
    //     title: task.name,
    //     start: task.startDate,
    //     end: task.dueDate,
    //     allDay: true,
    //     color: this.getPriorityColor(task.priority)
    //   };
    //   this.events.push(event);
    // }));
    this.events$ = this.tasks$
      .pipe(map(
        (tasks: Task[]) => tasks.map((task: Task) => {
          return {
            title: task.name,
            start: task.startDate,
            end: task.dueDate,
            allDay: true,
            color: this.getPriorityColor(task.priority),
            meta: {
              task
            }
          }
        })
      ))
  }

}
