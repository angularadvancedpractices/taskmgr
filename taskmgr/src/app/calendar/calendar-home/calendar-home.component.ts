import {Component, OnInit, ViewChild} from '@angular/core';
import * as fromRoot from '../../store/reducers';
import {Store} from "@ngrx/store";
import dayGridPlugin from '@fullcalendar/daygrid';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';
import {Observable} from "rxjs/index";
import {Task} from '../../domain';
import {map} from "rxjs/internal/operators";
import {FullCalendarComponent} from '@fullcalendar/angular';
import timeGrigPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import {EventInput} from '@fullcalendar/core';
import {MatDialog} from "@angular/material";
import {CalendarEventDialogComponent} from "../calendar-event-dialog/calendar-event-dialog.component";
import {getPriorityColor} from "src/app/utils/others.util";

@Component({
  selector: 'app-calendar-home',
  template: `
    <!--<div class='app-top'>-->
      <!--<button (click)='toggleVisible()'>toggle visible</button>-->
      <!--<button (click)='toggleWeekends()'>toggle weekends</button>-->
      <!--<button (click)='gotoPast()'>go to a date in the past</button>-->
      <!--(also, click a date/time to add an event)-->
    <!--</div>-->

    <div class='app-calendar' *ngIf="calendarVisible">
      <full-calendar
        #calendar
        defaultView="dayGridMonth"
        [header]="{
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
      }"
        [plugins]="calendarPlugins"
        [weekends]="calendarWeekends"
        [events]="events$|async"
        (eventClick)="handleEventClick($event)"
      ></full-calendar>
    </div>
  `,
  styles: [`    
    
    .app-calendar {
      margin: 10px auto;
      max-width: 900px;
      font-family: "Microsoft YaHei";
    }`]
})
export class CalendarHomeComponent implements OnInit {

  events$: any;
  tasks$: Observable<Task[]>;
  task: Task;
  eventColor: string;

  constructor(private store$: Store<fromRoot.State>,
              private matDialog: MatDialog) {
  }

  ngOnInit() {
    this.tasks$ = this.store$.select(fromRoot.getUserTasks);
    this.events$ = this.tasks$
      .pipe(map(tasks => tasks.filter(task => !task.completed).map(task => ({
          start: task.startDate,
          end: task.dueDate,
          title: task.name,
          id: task._id,
          color: getPriorityColor(task.priority)
    }))));
  }

  @ViewChild('calendar') calendarComponent: FullCalendarComponent; // the #calendar in the template

  calendarVisible = true;
  calendarPlugins = [dayGridPlugin, timeGrigPlugin, interactionPlugin];
  calendarWeekends = true;
  // calendarEvents: EventInput[] = [
  //   {title: 'Event Now', start: new Date()}
  // ];

  toggleVisible() {
    this.calendarVisible = !this.calendarVisible;
  }

  toggleWeekends() {
    this.calendarWeekends = !this.calendarWeekends;
  }

  gotoPast() {
    let calendarApi = this.calendarComponent.getApi();
    calendarApi.gotoDate('2000-01-01'); // call a method on the Calendar object
  }

  // handleDateClick(arg) {
  //   if (confirm('Would you like to add an event to ' + arg.dateStr + ' ?')) {
  //     this.calendarEvents = this.calendarEvents.concat({ // add new event data. must create new array
  //       title: 'New Event',
  //       start: arg.date,
  //       allDay: arg.allDay
  //     })
  //   }
  // }

  handleEventClick(arg) {
    const subscription = this.store$.select(fromRoot.getTaskById(arg.event.id)).subscribe(task => this.task = task);
    this.matDialog.open(CalendarEventDialogComponent, {
      width: '600px',
      data: {
        task: this.task,
        color: arg.event.backgroundColor
      }});
    subscription.unsubscribe();
  }

}
