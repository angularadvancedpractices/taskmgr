import { NgModule } from '@angular/core';
import { CalendarHomeComponent } from './calendar-home/calendar-home.component';
import {CalendarRoutingModule} from "./calendar-routing.module";
import {SharedModule} from "../shared/shared.module";
import { CalendarModule, DateAdapter } from 'angular-calendar';
import {adapterFactory} from "angular-calendar/date-adapters/date-fns";
import { DemoUtilsModule } from './demo-utils/module';
import { CalendarEventDialogComponent } from './calendar-event-dialog/calendar-event-dialog.component';

@NgModule({
  imports: [
    SharedModule,
    CalendarRoutingModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    DemoUtilsModule
  ],
  declarations: [CalendarHomeComponent, CalendarEventDialogComponent],
  entryComponents: [CalendarHomeComponent, CalendarEventDialogComponent]
})
export class CalendarsModule { }
