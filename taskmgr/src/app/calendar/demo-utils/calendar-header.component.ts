import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'mwl-demo-utils-calendar-header',
  template: `
    <mat-toolbar>
      <span>{{ viewDate | calendarDate: view + 'ViewTitle':locale }}</span>
      <button
        mat-icon-button
        mwlCalendarPreviousView
        [view]="view"
        [(viewDate)]="viewDate"
        (viewDateChange)="viewDateChange.next(viewDate)"
      >
        <mat-icon>chevron_left</mat-icon>
      </button>
      <button
        mat-button
        mwlCalendarToday
        [(viewDate)]="viewDate"
        (viewDateChange)="viewDateChange.next(viewDate)"
      >
        Today
      </button>
      <button
        mat-icon-button
        mwlCalendarNextView
        [view]="view"
        [(viewDate)]="viewDate"
        (viewDateChange)="viewDateChange.next(viewDate)"
      >
        <mat-icon>chevron_right</mat-icon>
      </button>
      <span style="flex: 1 1 auto"></span>

      <button
        mat-raised-button
        (click)="viewChange.emit('month')"
        [class.active]="view === 'month'"
      >
        Monthly View
      </button>
      <button
        mat-raised-button
        (click)="viewChange.emit('week')"
        [class.active]="view === 'week'"
      >
        Weekly View
      </button>
      <button
        mat-raised-button
        (click)="viewChange.emit('day')"
        [class.active]="view === 'day'"
      >
        Daily View
      </button>
      <br/>
    </mat-toolbar>
  `
})
export class CalendarHeaderComponent {
  @Input() view: string;

  @Input() viewDate: Date;

  @Input() locale: string = 'en';

  @Output() viewChange: EventEmitter<string> = new EventEmitter();

  @Output() viewDateChange: EventEmitter<Date> = new EventEmitter();
}
