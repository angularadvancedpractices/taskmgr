export const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  orange: {
    primary: '#FFAF38',
    secondary: '#D1E8FF'
  },
  grey: {
    primary: '#A6A6A6',
    secondary: '#FDF1BA'
  }
};
