import { NgModule } from '@angular/core';
import { CalendarModule } from 'angular-calendar';
import { CalendarHeaderComponent } from './calendar-header.component';
import {SharedModule} from "../../shared/shared.module";

@NgModule({
  imports: [CalendarModule, SharedModule],
  declarations: [CalendarHeaderComponent],
  exports: [CalendarHeaderComponent]
})
export class DemoUtilsModule {}
