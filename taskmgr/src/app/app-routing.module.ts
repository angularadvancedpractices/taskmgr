import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AuthGuardService} from './services/auth-guard.service';
import {NotFoundComponent} from './not-found/not-found.component';
import {ProjectHomeComponent} from './project/project-home/project-home.component';
import {LoginComponent} from './login/login/login.component';

const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent, pathMatch: 'full'},
  {path: 'project', redirectTo: '/project', pathMatch: 'full'},
  {path: 'tasks', redirectTo: 'tasks', pathMatch: 'full'},
  {path: 'calendar', redirectTo: 'calendar', pathMatch: 'full'},
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
