import {animate, state, style, transition, trigger} from '@angular/animations';

export const taskListItemAnimation = trigger('taskListItem', [
  state('out', style({'border-left-width': '3px'})),
  state('in', style({'border-left-width': '10px'})),
  transition('in => out', animate('100ms ease-in')),
  transition('out => in', animate('100ms ease-out'))
]);
