import {state, trigger, style, transition, animate} from '@angular/animations';

export const slideToRight = trigger('routeAnimation', [
  state('void', style({position: 'fixed', width: '100%', height: '85%'})),
  state('*', style({position: 'fixed', width: '100%', height: '85%'})),
  // 从没有状态到有状态，从右边进
  transition('void => *', [
    style({transform: 'translateX(-100%)'}),
    animate('.5s ease-in-out', style({transform: 'translateX(0)'}))
  ]),
  // 从左边进，右边出，从现在的状态到没有
  transition('* => void', [
    style({transform: 'translateX(0)'}),
    animate('.5s ease-in-out', style({transform: 'translateX(100%)'}))
  ])
]);
