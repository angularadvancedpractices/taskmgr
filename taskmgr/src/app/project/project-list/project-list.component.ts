import {ChangeDetectionStrategy, ChangeDetectorRef, Component, HostBinding, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {ProjectNewComponent} from '../project-new/project-new.component';
import {ProjectInviteComponent} from '../project-invite/project-invite.component';
import {ConfirmDialogComponent} from '../../shared/confirm-dialog/confirm-dialog.component';
import {slideToRight} from '../../animations/router.anim';
import {listAnimations} from '../../animations/list.anim';
import {Project} from '../../domain';
import * as _ from 'lodash';
import {filter, map, switchMap, take} from 'rxjs/internal/operators';
import {OnDestroy} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../store/reducers/index';
import * as projectActions from '../../store/actions/project.actions';
import {Observable} from 'rxjs/index';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss'],
  animations: [slideToRight, listAnimations],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectListComponent implements OnInit, OnDestroy {

  projects$: Observable<Project[]>;
  listAnim$: Observable<number>;
  // 路由动画是整个组件在切换的时候的动画相当于整个页面参加了这个动画所以应该在组件当中绑定
  // routeAnim是动画中定义的触发器
  @HostBinding('@routeAnimation') state;

  openNewProjectDialog() {
    const selectedImg = `/assets/img/covers/${Math.floor(Math.random() * 40)}_tn.jpg`;
    const dialogRef = this.dialog.open(ProjectNewComponent, {
      data: {
        thumbnails: this.getThumbnails(),
        image: selectedImg
      }
    });
    dialogRef.afterClosed().pipe(
      take(1),
      filter(n => n),
      map(val => ({...val, coverImg: this.buildImgSrc(val.coverImg)})))
      .subscribe(project => this.store$.dispatch(new projectActions.AddProject(project)));
    // switchMap(v => this.projectService$.add(v)),
    // ).subscribe(project => {
    //   this.projects = [...this.projects, project];
    //   this.ref.markForCheck();
    // });
    // 所以这里有两个流，第一个是dialog返回的subscribe，第二个就是projectService本身的这个流
    // 需要将两个流拍扁，使用switchMap将projectService的流和dialog的流合并成一个流再进行订阅
    // 一般不在subscribe中再subscribe
    // dialogRef.afterClosed().subscribe(project => {
    //   this.projectService$.add(project).subscribe(p => console.log(p));
    //   // this.ref.markForCheck();
    // });
  }

  // todo: check if the logged in user is deleting himself and if yes, then don't show this project in the project list
  openInviteDialog(project: Project) {
    let members = [];
    this.store$.select(fromRoot.getProjectUsers(project._id))
      .subscribe(m => {
        members = m;
      });
    const dialogRef = this.dialog.open(ProjectInviteComponent, {data: {members: members}});
    dialogRef.afterClosed()
      .pipe(
        take(1)
      )
      .subscribe(val => {
        if (val) {
          // console.log(val);
          this.store$.dispatch(new projectActions.InviteProject({projectId: project._id, members: val}));
          this.projects$ = this.store$.select(fromRoot.getProjects);
        }
      });
    // const dialogRef = this.dialog.open(ProjectInviteComponent, {data: {members: []}});
    // dialogRef.afterClosed().subscribe(ref => console.log(ref));
  }

  openDeleteProjectDialog(project) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Delete Project',
        content: 'Do you want to delete this project?'
      }
    });
    console.log(project);
    // dialogRef.afterClosed().pipe(
    //   take(1),
    //   filter(n => n),
    //   // switchMap(_ => this.projectService$.del(project))
    // )
    //   .subscribe(x => this.store$.dispatch(new projectActions.DeleteProject(project)));
    // .subscribe(result => {
    //   // const index = this.projects.findIndex(p => p.id === result.id);
    //   this.projects = this.projects.filter(p => p.id !== result.id);
    //   this.ref.markForCheck();
    // });
  }

  openEditProjectDialog(project) {
    this.dialog.open(ProjectNewComponent, {data: {project: project, thumbnails: this.getThumbnails()}})
      .afterClosed().pipe(
      take(1),
      filter(n => n),
      map(val => ({...val, _id: project._id, coverImg: this.buildImgSrc(val.coverImg)}))
      // switchMap(v => this.projectService$.update(v))
    )
      .subscribe(p => this.store$.dispatch(new projectActions.UpdateProject(p)));
    // result is the project returned from project update
    //   .subscribe(result => {
    // const index = this.projects.map(p => p.id).indexOf(result.id);
    // this.projects = [...this.projects.slice(0, index), result, ...this.projects.slice(index + 1)];
    // replace the project from update service with the one in the initialized project lists
    //   const index = this.projects.findIndex(p => p.id === result.id);
    //   this.projects[index] = result;
    //   this.ref.markForCheck();
    // });
  }

  onProjectSelected(project: Project) {
    this.store$.dispatch(new projectActions.SelectProject(project));
  }

  constructor(private dialog: MatDialog,
              private ref: ChangeDetectorRef,
              private store$: Store<fromRoot.State>) {
  }

  ngOnInit() {
    this.store$.dispatch(new projectActions.LoadProject({}));
    this.projects$ = this.store$.select(fromRoot.getProjects);
    this.listAnim$ = this.projects$.pipe(map(p => {
      return p.length;
    }));
  }

  ngOnDestroy(): void {
  }

  private getThumbnails() {
    return _.range(0, 40)
      .map(i => `/assets/img/covers/${i}_tn.jpg`);
  }

  private buildImgSrc(img: string): string {
    return img.indexOf('_') > -1 ? img.split('_')[0] + '.jpg' : img;
  }
}
