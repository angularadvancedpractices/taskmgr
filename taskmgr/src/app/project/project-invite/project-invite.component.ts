import {ChangeDetectionStrategy, Component, Inject, OnInit} from '@angular/core';
import {RootInjectChildService} from '../../root-inject-child.service';
import {OverlayContainer} from '@angular/cdk/overlay';
import {User} from '../../domain/user.model';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-project-invite',
  templateUrl: './project-invite.component.html',
  styleUrls: ['./project-invite.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectInviteComponent implements OnInit {
  members: User[] = [];

  ngOnInit(): void {
    this.members = this.data.members;
  }

  onSubmit(ev: Event, {valid, value}) {
    ev.preventDefault();
    if (!valid) {
      return;
    }
    this.dialogRef.close(this.members);
  }

  constructor(@Inject(MAT_DIALOG_DATA) private data,
              private dialogRef: MatDialogRef<ProjectInviteComponent>) {
  }

  // constructor(private sharedService: RootInjectChildService,
  //             private overlayContainer: OverlayContainer) {
  // }

  // ngOnInit() {
  // this.sharedService.darkTheme ?
  //   this.overlayContainer.getContainerElement().classList.add('myapp-dark-theme') :
  //   this.overlayContainer.getContainerElement().classList.remove('myapp-dark-theme');
  // }

}
