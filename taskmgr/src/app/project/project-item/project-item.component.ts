import {Component, EventEmitter, HostBinding, HostListener, Input, OnInit, Output} from '@angular/core';
import {projectCardAnimations} from '../../animations/card.anim';

@Component({
  selector: 'app-project-item',
  templateUrl: './project-item.component.html',
  styleUrls: ['./project-item.component.scss'],
  animations: [
    projectCardAnimations
  ]
})
export class ProjectItemComponent implements OnInit {

  @Input() item;
  @Output() inviteMemberEvent = new EventEmitter<void>();
  @Output() deleteProjectEvent: EventEmitter<void> = new EventEmitter();
  @Output() editProjectEvent: EventEmitter<void> = new EventEmitter();
  @Output() selected: EventEmitter<void> = new EventEmitter();

  @HostBinding('@projectCard') projectCardState = 'out';

  @HostListener('mouseenter')
  onMouseEnter() {
    this.projectCardState = 'hover';
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    this.projectCardState = 'out';
  }

  constructor() {
  }

  ngOnInit() {
  }

  onInviteClick(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    this.inviteMemberEvent.emit();
  }

  onDeleteClick(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    this.deleteProjectEvent.emit();
  }

  onEditClick(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    this.editProjectEvent.emit();
  }

  onClick() {
    this.selected.emit();
  }

}
