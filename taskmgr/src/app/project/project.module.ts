import { NgModule } from '@angular/core';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectItemComponent } from './project-item/project-item.component';
import { ProjectNewComponent } from './project-new/project-new.component';
import { ProjectInviteComponent } from './project-invite/project-invite.component';
import {SharedModule} from '../shared/shared.module';
import {ProjectRoutingModule} from './project-routing.module';
import { ProjectHomeComponent } from './project-home/project-home.component';

@NgModule({
  imports: [
    SharedModule,
    ProjectRoutingModule
  ],
  declarations: [ProjectListComponent, ProjectItemComponent, ProjectNewComponent, ProjectInviteComponent, ProjectHomeComponent],
  entryComponents: [
    ProjectNewComponent, ProjectInviteComponent
  ]
})
export class ProjectModule { }
