import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-project-new',
  templateUrl: './project-new.component.html',
  styleUrls: ['./project-new.component.scss']
})
export class ProjectNewComponent implements OnInit {
  title = '';
  coverImages = [];
  form: FormGroup;
  projectTitle: string;
  project: any;
  constructor(@Inject(MAT_DIALOG_DATA) private data,
              private dialogRef: MatDialogRef<any>,
              private fb: FormBuilder) {
  }

  ngOnInit() {
    this.coverImages = this.data.thumbnails;
    if (this.data.project) {
      this.form = this.fb.group({
        name: [this.data.project.name, Validators.required],
        desc: [this.data.project.desc],
        coverImg: [this.data.project.coverImg]
      });
      this.title = 'Edit Project';
    } else {
      this.form = this.fb.group({
        name: ['', Validators.required],
        desc: ['', Validators.required],
        coverImg: [this.data.image]
      });
      this.title = 'Create Project';
    }
  }

  onSubmit({value, valid}, event) {
    event.preventDefault();
    if (!valid) {
      return;
    }
    this.dialogRef.close(value);
  }

}
