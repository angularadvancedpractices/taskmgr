import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ProjectListComponent} from './project-list/project-list.component';
import {AuthGuardService} from '../services/auth-guard.service';
import {ProjectHomeComponent} from './project-home/project-home.component';

const routes: Routes = [
  {
    path: 'project',
    canActivate: [AuthGuardService],
    component: ProjectListComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectRoutingModule {
}
