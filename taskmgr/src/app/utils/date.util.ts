import * as is_date from 'date-fns/is_date';
import * as is_valid from 'date-fns/is_valid';
import * as is_future from 'date-fns/is_future';
import * as difference_in_years from 'date-fns/difference_in_years';
import * as parse from 'date-fns/parse';

export const isValidDate = (val: string): boolean => {
  const date = parse(val);
  return is_date(date) && is_valid(date) && !is_future(date) && difference_in_years(Date.now(), date) < 150;
};
