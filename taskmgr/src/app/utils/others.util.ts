export const getPriorityColor = (priority) => {
  if (priority === 1) {
    return '#FF0000';
  } else {
    return priority === 2 ? '#FFAF38' : '#A6A6A6';
  }
};
