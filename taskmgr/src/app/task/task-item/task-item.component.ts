import {Component, EventEmitter, HostListener, Input, OnInit, Output} from '@angular/core';
import {taskListItemAnimation} from '../../animations/item.anim';

@Component({
  selector: 'app-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.scss'],
  animations: [
    taskListItemAnimation
  ]
})
export class TaskItemComponent implements OnInit {

  @Input() item;
  @Input() avatar;
  @Output() itemClickEvent: EventEmitter<void> = new EventEmitter();
  @Output() itemCheckEvent: EventEmitter<void> = new EventEmitter();

  widerPriority = 'out';

  constructor() {
  }

  ngOnInit() {
    this.avatar = this.avatar ? this.avatar : 'unassigned';
  }

  onClick(ev: Event) {
    ev.preventDefault();
    this.itemClickEvent.emit();
  }

  onCheckboxClick(ev: Event) {
    ev.stopPropagation();
  }

  onChange() {
    this.itemCheckEvent.emit();
  }

  @HostListener('mouseenter')
  onMouseEnter() {
    this.widerPriority = 'in';
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    this.widerPriority = 'out';
  }

}
