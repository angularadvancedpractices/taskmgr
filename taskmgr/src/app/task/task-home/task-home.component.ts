import {Component, HostBinding, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {TaskNewComponent} from '../task-new/task-new.component';
import {TaskCopyComponent} from '../task-copy/task-copy.component';
import {TaskListNewComponent} from '../task-list-new/task-list-new.component';
import {ConfirmDialogComponent} from '../../shared/confirm-dialog/confirm-dialog.component';
import {TaskEditComponent} from '../task-edit/task-edit.component';
import {slideToRight} from '../../animations/router.anim';
import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../store/reducers';
import {from, Observable} from 'rxjs/index';
import {TaskList} from '../../domain/task-list.model';
import {filter, switchMap, take, map, tap, withLatestFrom} from 'rxjs/internal/operators';
import * as taskListActions from '../../store/actions/task-list.actions';
import * as taskActions from '../../store/actions/task.actions';
import {Task} from '../../domain/task.model';

@Component({
  selector: 'app-task-home',
  templateUrl: './task-home.component.html',
  styleUrls: ['./task-home.component.scss'],
  animations: [slideToRight]
})
export class TaskHomeComponent implements OnInit {

  @HostBinding('@routeAnimation') state;

  projectId$: Observable<string>;
  lists$: Observable<any>;
  private projectId;

  constructor(private matDialog: MatDialog,
              private router: Router,
              private route: ActivatedRoute,
              private store$: Store<fromRoot.State>) {
  }

  ngOnInit() {
    this.projectId$ = this.route.params['id'];
    this.projectId = this.route.snapshot.params['id'];
    this.lists$ = this.store$.select(fromRoot.getListsWithTasks);
  }

  openNewTaskDialog(list) {
    // console.log(list);
    const user$ = this.store$.select(fromRoot.getAuthUser);
    user$
      .pipe(take(1), withLatestFrom(this.store$.select(fromRoot.getProjectUsers(this.projectId))))
      .subscribe(([user, projectUsers]) => {
        // console.log(user);
        // console.log(projectUsers);
        const dialogRef = this.matDialog.open(TaskNewComponent, {data: {owner: user, projectUsers: projectUsers}});
        dialogRef.afterClosed()
          .pipe(
            take(1),
            filter(n => n)
          )
          .subscribe(val => {
            this.store$.dispatch(new taskActions.AddTask({
              task: {
                ...val,
                taskListId: list._id,
                completed: false,
                createDate: new Date()
              },
              taskListId: list._id
            }));
          });
      });
  }

  openMoveAllTaskDialog(list) {
    const dialogRef = this.matDialog.open(TaskCopyComponent, {
      data: {
        lists: this.lists$,
        currentList: list,
        title: 'Move Task List'
      }
    });
    dialogRef.afterClosed().pipe(
      take(1),
      filter(n => n)
    ).subscribe(targetList => this.store$.dispatch(new taskActions.MoveAll({
      srcListId: list._id,
      targetListId: targetList.selectedList._id
    })));
  }

  openEditTaskDialog(task) {
    const $user = this.store$.select(fromRoot.getAuthUser);
    $user
      .pipe(take(1), withLatestFrom(this.store$.select(fromRoot.getProjectUsers(this.projectId))))
      .subscribe(([user, projectUsers]) => {
        console.log(user);
        console.log(projectUsers);
        const ref$ = this.matDialog.open(TaskEditComponent, {
          data: {
            title: 'Edit Task',
            task: task,
            owner: user,
            projectUsers: projectUsers
          }
        });
        ref$.afterClosed()
          .pipe(take(1), filter(n => n))
          .subscribe(t => this.store$.dispatch(new taskActions.UpdateTask(t)));
      });
  }

  onTaskCheck(task) {
    this.store$.dispatch(new taskActions.CompleteTask(task));
  }

  openEditTaskListDialog(list: TaskList) {
    const dialogRef = this.matDialog.open(TaskListNewComponent, {data: {title: 'Edit Task List', taskList: list}});
    dialogRef.afterClosed()
      .pipe(
        take(1)
      )
      // the result here is the tasklist form
      .subscribe(result => this.store$.dispatch(new taskListActions.UpdateTaskList({...result, _id: list._id})));
  }

  openDeleteTaskListDialog(list: TaskList) {
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Delete Task List',
        content: 'Do you want to delete this task list?'
      }
    });
    dialogRef.afterClosed()
      .pipe(
        take(1),
        filter(n => n),
      )
      .subscribe(_ => this.store$.dispatch(new taskListActions.DeleteTaskList({
        taskList: list,
        projectId: this.projectId
      })));
  }

  openNewTaskListDialog() {
    const dialogRef = this.matDialog.open(TaskListNewComponent, {data: {title: 'Create Task List'}});
    dialogRef.afterClosed()
      .pipe(
        take(1),
        filter(n => n),
        withLatestFrom(this.store$.select(fromRoot.getMaxListOrder), (n, o) => {
          return {
            name: n.name,
            order: o + 1
          };
        })
      )
      .subscribe((result) => this.store$.dispatch(new taskListActions.AddTaskList({
        ...result,
        projectId: this.projectId
      })));
  }

  handleMove(srcData, targetList: TaskList) {
    switch (srcData.tag) {
      case 'task-item':
        const sourceTask = srcData.data as Task;
        console.log(sourceTask);
        console.log(targetList);
        this.store$.dispatch(new taskActions.MoveTask({task: sourceTask, targetListId: targetList._id}));
        break;
      case 'task-list': {
        const sourceTaskList = srcData.data as TaskList;
        this.store$.dispatch(new taskListActions.SwapTaskList([sourceTaskList, targetList]));
        break;
      }
      default:
        break;
    }
  }

}
