import {NgModule} from '@angular/core';
import {TaskHomeComponent} from './task-home/task-home.component';
import {TaskListComponent} from './task-list/task-list.component';
import {TaskItemComponent} from './task-item/task-item.component';
import {TaskHeaderComponent} from './task-header/task-header.component';
import {SharedModule} from '../shared/shared.module';
import {TaskRoutingModule} from './task-routing.module';
import {TaskNewComponent} from './task-new/task-new.component';
import {TaskCopyComponent} from './task-copy/task-copy.component';
import {TaskListNewComponent} from './task-list-new/task-list-new.component';
import {TaskEditComponent} from './task-edit/task-edit.component';
import { TaskMyComponent } from './task-my/task-my.component';

@NgModule({
  imports: [
    SharedModule,
    TaskRoutingModule,
  ],
  declarations: [TaskHomeComponent, TaskListComponent, TaskItemComponent, TaskHeaderComponent,
    TaskNewComponent, TaskCopyComponent, TaskListNewComponent, TaskEditComponent, TaskMyComponent],
  entryComponents: [TaskNewComponent, TaskCopyComponent, TaskListNewComponent, TaskEditComponent]
})
export class TaskModule {
}
