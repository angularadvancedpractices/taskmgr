import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-task-list-new',
  template: `
    <form [formGroup]="form" (submit)="onSubmit(form)">
      <h2 mat-dialog-title>{{dialogTitle}}</h2>
      <div mat-dialog-content>
        <mat-form-field class="full-width">
          <input matInput type="text" placeholder="Task List Name" formControlName="name">
        </mat-form-field>
      </div>
      <div mat-dialog-actions>
        <button mat-raised-button type="submit" [disabled]="!form.valid">Save</button>
        <button mat-button type="button" mat-dialog-close>Close</button>
      </div>
    </form>
  `,
  styles: [`
    .full-width {
      width: 100%;
    }
  `]
})
export class TaskListNewComponent implements OnInit {

  dialogTitle;
  form: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) private dialogData: any,
              private fb: FormBuilder,
              private dialogRef: MatDialogRef<TaskListNewComponent>) {
  }

  ngOnInit() {
    this.dialogTitle = this.dialogData.title;
    this.form = this.fb.group({
      name: [this.dialogData.taskList ? this.dialogData.taskList.name : '', Validators.required]
    });
  }

  onSubmit({value, valid}) {
    if (!valid) {
      return;
    }
    this.dialogRef.close(value);
  }
}
