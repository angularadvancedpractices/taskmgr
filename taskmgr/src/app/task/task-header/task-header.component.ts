import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-task-header',
  templateUrl: './task-header.component.html',
  styleUrls: ['./task-header.component.scss']
})
export class TaskHeaderComponent implements OnInit {

  @Input() header = '';
  @Output() newTaskEmitter: EventEmitter<void> = new EventEmitter<void>();
  @Output() moveAllTaskEmitter: EventEmitter<void> = new EventEmitter();
  @Output() editTaskListEmitter: EventEmitter<void> = new EventEmitter();
  @Output() deleteTaskListEmitter: EventEmitter<void> = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  onNewTaskClick() {
    this.newTaskEmitter.emit();
  }

  onMoveAllClick() {
    this.moveAllTaskEmitter.emit();
  }

  onEditClick() {
    this.editTaskListEmitter.emit();
  }

  onDeleteClick() {
    this.deleteTaskListEmitter.emit();
  }
}
