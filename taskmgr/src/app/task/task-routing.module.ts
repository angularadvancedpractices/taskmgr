import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {TaskHomeComponent} from './task-home/task-home.component';
import {AuthGuardService} from "../services/auth-guard.service";
import {TaskMyComponent} from "./task-my/task-my.component";

const routes: Routes = [
  {
    path: 'tasks/me', pathMatch: 'full', component: TaskMyComponent, canActivate: [AuthGuardService]
  },
  {
    path: 'tasks/:id', component: TaskHomeComponent, canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaskRoutingModule {
}
