import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from "../../domain/user.model";

@Component({
  selector: 'app-task-edit',
  templateUrl: './task-edit.component.html',
  styleUrls: ['./task-edit.component.scss']
})
export class TaskEditComponent implements OnInit {

  dialogTitle: string;
  priorities: any[];
  selectedPriority: number;
  form: FormGroup;
  projectUsers: User[];
  owner: User;

  constructor(@Inject(MAT_DIALOG_DATA) private dialogData,
              private dialogRef: MatDialogRef<TaskEditComponent>,
              private fb: FormBuilder,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.projectUsers = this.dialogData.projectUsers;
    this.owner = this.dialogData.owner;
    console.log(this.dialogData);
    this.priorities = [
      {'name': 'emergency', 'priority': 1},
      {'name': 'important', 'priority': 2},
      {'name': 'normal', 'priority': 3},
    ];
    this.selectedPriority = this.dialogData.task.priority;
    this.form = this.fb.group({
      name: [this.dialogData.task.name, Validators.required],
      description: [this.dialogData.task.description, Validators.required],
      priority: [this.dialogData.task.priority, Validators.required],
      startDate: [this.dialogData.task.startDate, Validators.required],
      participants: [this.dialogData.task.participants, Validators.required],
      // 包裹了一个数组为了传入chiplist的时候能够适配
      owner: [[this.dialogData.owner]],
      dueDate: [this.dialogData.task.dueDate, Validators.required],
      remark: [this.dialogData.task.remark]
    });
    this.dialogTitle = this.dialogData.title;
  }

  onSubmit(ev: Event, {valid, value}) {
    ev.preventDefault();
    // 转换成相同的格式
    const startDate = new Date(value.startDate);
    const dueDate = new Date(value.dueDate);
    if (!valid) {
      return;
    } else if (startDate > dueDate) {
      this.snackBar.open('due date cannot be before the start date', 'dismiss', {
        duration: 2000,
      });
      return;
    }
    this.dialogRef.close({
      ...value,
      _id: this.dialogData.task._id,
      ownerId: value.owner.length > 0 ? value.owner[0]._id : null,
      participantIds: value.participants.map(f => f._id)
    });
  }

}
