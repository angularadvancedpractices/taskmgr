import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../../domain/user.model';

@Component({
  selector: 'app-task-new',
  templateUrl: './task-new.component.html',
  styleUrls: ['./task-new.component.scss']
})
export class TaskNewComponent implements OnInit {

  form: FormGroup;
  priorities: any[];
  dialogTitle: string;
  projectUsers: User[];

  constructor(@Inject(MAT_DIALOG_DATA) private dialogData: any,
              private dialogRef: MatDialogRef<TaskNewComponent>,
              private fb: FormBuilder,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    const task = this.dialogData.task;
    this.projectUsers = this.dialogData.projectUsers;
    // if task is not null, then it is modify the task, if is null then is new task
    this.form = this.fb.group({
      name: [task ? this.dialogData.task.name : '', Validators.required],
      desc: [task ? this.dialogData.task.description : '', Validators.required],
      priority: [task ? this.dialogData.task.priorities : 3, Validators.required],
      owner: [task ? [this.dialogData.task.ownerId] : [this.dialogData.owner]],
      participants: [task ? [this.dialogData.task.participantIds] : [], Validators.required],
      startDate: [task ? this.dialogData.task.createDate : new Date(), Validators.required],
      dueDate: [task ? this.dialogData.task.dueDate : new Date, Validators.required],
      remark: [task ? this.dialogData.task.remark : '']
    });
    this.dialogTitle = this.dialogData.title;
    this.priorities = [
      {'name': 'emergency', 'priority': 1},
      {'name': 'important', 'priority': 2},
      {'name': 'normal', 'priority': 3},
    ];
  }
  checkDate(startPicker) {
    console.log(startPicker);
  }
  onSubmit(ev: Event, {value, valid}) {
    ev.preventDefault();
    const startDate = value.startDate;
    const dueDate = value.dueDate;
    if (!valid) {
      return;
    } else if (startDate > dueDate) {
      this.snackBar.open('due date cannot be before the start date', 'dismiss', {
        duration: 2000,
      });
      return;
    }
    this.dialogRef.close({
      ...value,
      ownerId: value.owner.length > 0 ? value.owner[0]._id : null,
      participantIds: value.participants.map(f => f._id)
    });
  }

}
