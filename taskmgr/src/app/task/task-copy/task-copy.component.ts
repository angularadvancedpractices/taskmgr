import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Observable} from 'rxjs/index';
import {TaskList} from '../../domain/task-list.model';
import {map} from 'rxjs/internal/operators';

@Component({
  selector: 'app-task-copy',
  templateUrl: './task-copy.component.html',
  styleUrls: ['./task-copy.component.scss']
})
export class TaskCopyComponent implements OnInit {

  taskLists$: Observable<TaskList[]>;
  currentList: TaskList;
  dialogTitle: string;
  selectedList: TaskList;


  constructor(@Inject(MAT_DIALOG_DATA) private data,
              private dialogRef: MatDialogRef<TaskCopyComponent>) {
  }

  ngOnInit() {
    this.currentList = this.data.currentList;
    this.taskLists$ = this.data.lists.pipe(
      map((lists: TaskList[]) => lists.filter(list => list._id !== this.currentList._id))
    );
    this.dialogTitle = this.data.title;
  }

  submit(ev: Event, form) {
    ev.preventDefault();
    this.dialogRef.close({selectedList: form.value.selectedList});
  }

  onClick() {
    console.log('canceled');
  }

}
