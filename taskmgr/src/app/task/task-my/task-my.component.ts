import {Component, OnInit} from '@angular/core';
import * as fromRoot from '../../store/reducers';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/index';
import {Task} from '../../domain/task.model';
import {User} from '../../domain/user.model';
import {filter, map, take, withLatestFrom} from 'rxjs/internal/operators';
import {getUserById, getUsersByIds} from '../../store/reducers/index';
import {MatDialog} from "@angular/material";
import {TaskEditComponent} from "../task-edit/task-edit.component";

import * as taskActions from '../../store/actions/task.actions';
import * as taskListActions from '../../store/actions/task-list.actions';
import {Project} from "../../domain/project.model";

@Component({
  selector: 'app-task-my',
  templateUrl: './task-my.component.html',
  styleUrls: ['./task-my.component.scss']
})
export class TaskMyComponent implements OnInit {

  tasks$: Observable<Task[]>;
  items$: Observable<any[]>;

  constructor(private store$: Store<fromRoot.State>,
              private matDialog$: MatDialog) {
  }

  onClick(ev: Event, task: Task) {
    ev.preventDefault();
    let project: Project;
    let projectUsers: User[];
    let taskOwner: User;
    let participants: User[];
    console.log(task);
    this.store$.select(fromRoot.getProjectByTask(task)).pipe(take(1)).subscribe(p => project = p);
    console.log(project);
    this.store$.select(fromRoot.getProjectUsers(project._id)).pipe(take(1)).subscribe(pu => projectUsers = pu);
    this.store$.select(fromRoot.getUserById(task.owner)).pipe(take(1)).subscribe(u => taskOwner = u);
    this.store$.select(fromRoot.getUsersByIds(task.participants)).pipe(take(1)).subscribe(us => participants = us);
    task = {...task, participants: participants};
    const ref$ = this.matDialog$.open(TaskEditComponent, {
      data: {
        title: 'Edit Task',
        task: task,
        owner: taskOwner,
        projectUsers: projectUsers
      }
    });
    ref$.afterClosed()
      .pipe(take(1), filter(n => n))
      .subscribe(t => this.store$.dispatch(new taskActions.UpdateTask(t)));
  }

  ngOnInit() {
    this.store$.dispatch(new taskListActions.LoadAllTaskLists(null));
    this.store$.dispatch(new taskActions.LoadAllTasks(null));
    this.tasks$ = this.store$.select(fromRoot.getUserTasks);
    this.items$ = this.tasks$.pipe(
      map(tasks => tasks.map(task => {
        const t: { avatars: string[], task: Task } = {avatars: [], task};
        this.store$.select(getUserById(task.owner)).pipe(
          take(1),
          withLatestFrom(this.store$.select(getUsersByIds(task.participants)))
        )
          .subscribe(([owner, participants]) => {
            const filtered = participants.filter(p => p._id !== owner._id).map(p => p.avatar);
            t.avatars = [...t.avatars, owner.avatar];
            t.avatars = [...t.avatars, ...filtered];
          });
        return t;
      }))
    );
  }

}
