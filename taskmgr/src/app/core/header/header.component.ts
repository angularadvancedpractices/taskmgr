import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Store} from '@ngrx/store';
import {getAuthState, State} from '../../store/reducers/index';
import {Observable} from 'rxjs/index';
import {Auth} from '../../domain/auth.model';
import * as auth from '../../store/actions/auth.actions';
import {AuthState} from '../../store/reducers/auth.reducer';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Output() toggle = new EventEmitter();
  @Output() toggleDarkTheme = new EventEmitter();

  authState$: Observable<AuthState>;

  constructor(private store$: Store<State>) {
  }

  ngOnInit() {
    this.authState$ = this.store$.select(getAuthState);
  }

  openSideBar() {
    this.toggle.emit();
  }

  onChange(checked: boolean) {
    this.toggleDarkTheme.emit(checked);
  }

  logout() {
    this.store$.dispatch(new auth.Logout(null));
  }

}
