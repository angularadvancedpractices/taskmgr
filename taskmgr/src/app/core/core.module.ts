import {NgModule, SkipSelf, Optional} from '@angular/core';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {SidebarComponent} from './sidebar/sidebar.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {loadSvgResources} from '../utils/load-svg-resources';
import {BrowserModule, DomSanitizer} from '@angular/platform-browser';
import {SharedModule} from '../shared/shared.module';
import {MatIconRegistry} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from '../app-routing.module';
import '../utils/debug.util';
import {StoreModule} from '@ngrx/store';
import * as reducers from '../store/reducers';
import {EffectsModule} from '@ngrx/effects';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {QuoteEffects} from '../store/effects/quote.effects';
import {AuthEffects} from '../store/effects/auth.effects';
import {ProjectEffects} from '../store/effects/project.effects';
import {TaskListEffects} from '../store/effects/task-list.effects';
import {TaskEffects} from '../store/effects/task.effects';
import {UserEffects} from '../store/effects/user.effects';
import {metaReducers} from '../store/reducers/index';
import {JwtInterceptor} from '../services/jwt.interceptor';

@NgModule({
  imports: [
    AppRoutingModule,
    SharedModule,
    HttpClientModule,
    BrowserAnimationsModule,
    BrowserModule,
    StoreModule.forRoot(reducers.reducers, {metaReducers}),
    // StoreModule.forRoot(reducers.reducers),
    StoreModule.forFeature('root', reducers.reducers),
    StoreDevtoolsModule.instrument(),
    EffectsModule.forRoot([
      QuoteEffects,
      AuthEffects,
      ProjectEffects,
      TaskListEffects,
      TaskEffects,
      UserEffects,
    ])
  ],
  declarations: [HeaderComponent, FooterComponent, SidebarComponent],
  exports: [HeaderComponent, FooterComponent, SidebarComponent, AppRoutingModule, BrowserModule],
  providers: [
    {
      provide: 'BASE_CONFIG',
      useValue: {
        // uri: 'http://localhost:3000'
        uri: 'http://178.157.90.179:3002'
      }
    },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parent: CoreModule, iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    if (parent) {
      throw new Error('module exists, no reloading~');
    }
    loadSvgResources(iconRegistry, sanitizer);
  }
}
